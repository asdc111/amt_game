<? ob_start();?>
<?php
//session_set_cookie_params(0);
/*session_start();
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
    // last request was more than 30 minutes ago
    session_unset();     // unset $_SESSION variable for the run-time 
    session_destroy();   // destroy session data in storage
}
$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
if(isset($_SESSION['passcode']) )
{
    //header("Location:content.php");
}
else
{
     header("Location:index.php");
}

    
if($_SESSION['visitedSampleGame']) 
    $BtnLabel ='Start the Game'; 
else 
    $BtnLabel ='Start Sample Game'; 
*/
?>

<html>

<head>
  <title>Teamcore Network Security Game</title>
  <meta charset=utf-8 />
  <style>
    div.container {
      display:inline-block;
    }


  </style>
</head>
</head>
<body>
    <img src="logo.jpg">
    <script type="text/javascript" src="jquery-1.11.1.min.js"></script>
    <!--<script type="text/javascript">
                flag=true;
                window.onbeforeunload = function(){
                    if(flag)
                    {      $.get("logout.php");
                      //  return "Warning you are still logged in, are you sure?";
                    }
                    };
</script> -->
       <!-- <form action="content1.php" method="post" onsubmit="flag=false;"> -->
<H1>Maximizing Influence in a Multiple Steps Under Uncertainty</H1>
<H2><u>Context</u></H2>
<p align="justify">Homeless youth are at high risk from HIV-AIDS (the dangerous disease) because of activities like sharing drug needles etc., which are very common among homeless youth. Thus, many social workers (and associated agencies) are planning to conduct two intervention camps for homeless youth in order to raise
awareness about HIV among homeless youth. However, since social workers are few in number, they can only reach out to a couple of homeless youth in their planned intervention camps. Their plan to encourage attendees of their intervention camp to raise awareness about HIV among all their peers. Therefore, the social workers 
have to decide which homeless youth should be invited to participate in their intervention camps. Intuitively, they want to invite the <it>"most influential"</it> homeless youth, i.e., youth who will be able to raise awareness among as many homeless youth as possible (or youth who will maximize the spread of awareness 
about HIV). To aid them in this decision, they have been given information about a social network which shows all the friendships between all the homeless youth. By looking at this social network (which will be described next), the social workers have to decide which youth from this network 
should be invited for their two intervention camps. Since our social workers are exhausted from a tough day at work, they have requested you to make this decision for them. </p>


<H2><u>Social Network Description</u></H2>
<p align="justify">The social network represents a map of friendships between different homeless youth. In this social network, each homeless youth is represented by a circle similar to the one shown in the figure below. The letter inside the circle represents the name of that particular homeless youth. For example, the name of the
homeless youth in the figure is <b>M</b>. Moreover, each friendship in the social network is represented by a solid line that connects two circles. For example, the figure below shows a line between two circles <b>E</b> and <b>D</b>, which represents that the homeless youth corresponding to circles <b>E</b> and <b>D</b> are friends.
Moreover, each friendship in the social network has an associated strength, which is a percentage from 0 to 100%. For example, in the figure below, the friendship between <b>B</b> and <b>C</b> has a strength of 40%.
This means that if <b>E</b> is aware about HIV and <b>D</b> is not, then with a 40% chance he will make <b>D</b> aware about HIV as well. Similarly, if <b>D</b> is aware about HIV and <b>E</b> is not, then with a 40% chance he will make <b>E</b> aware about HIV as well. 
An estimate of the strength of all friendships in the network is provided to you (explained more in the observation model below).
</p> 

<div class="container">
<img src="../images/GameScreenshots/SourceNode.png" />
<p align="center">Network Node</p>
</div>
<div class="container">
<img src="../images/GameScreenshots/uncert-edge.png" />
<p align="center">Network Edge</p>
</div>





<H2><u>Influence Model</u></H2>
<p align="justify">In order to help you in your task, the social workers tell you how the attendees of their intervention camp will raise awareness among their peers. According to them, if circle <b>E</b> is invited for the intervention, then he/she will raise awareness about HIV among 
all homeless youth which are his friends in the social network. This awareness spread will occur according to the strength of the friendships between <b>E</b> and his/her friends. This means that it is possible that <b>E</b> may fail at spreading awareness among some of his friends and 
may succeed at spreading awareness among some others. Next, all the friends of <b>E</b> which were made aware of HIV will then potentially raise awareness among their friends. In other words, everyone withing two hops of <b>E</b> will potentially be made aware about HIV. It is important 
to note that not all people within two hops of <b>E</b> will become aware about HIV. For example, the figure below shows an example of a network in which circle <b>E</b> was invited for the intervention (denoted by the red color of circle <b>E</b>), then he/she will try to raise awareness
among his/her friends (denoted by green circles) using the strength of friendships that connect <b>E</b> to his/her friends. Next, all these friends will then to try to spread awareness among their friends who are not aware about HIV (denoted by blue) according to the strength of their 
friendships with their friends.   


<div class="container">
<img src="../images/GameScreenshots/uncert-1.png" />
<p align="center">Sample Network</p>
</div>
<div class="container">
<img src="../images/GameScreenshots/uncert-2.png" />
<p align="center">Influence spread by circle E</p>
</div>
<div class="container">
<img src="../images/GameScreenshots/uncert-3.png" />
<p align="center">Influence spread by friends of E</p>
</div>

<H2><u>Observation Model</u></H2>
<p align="justify">Since finding out the exact strength of all friendships is impractical, the social workers provide us an estimate of the strength of friendships. After conducting the first intervention, the social workers talk to the invited homeless youth and infer the actual values of the strengths 
of friendships that connect the invited youth to other homeless youth. More specifically, for each youth that was invited, we find out the whether the true strength of each of his/her friendships is 100% or 0%. For example, in the figure below, youth <b>F</b> was invited into the intervention. After the 
intervention, we find out whether the true strength of edges <b>(F, C)</b> and <b>(F, G)</b> is 100% or 0%. If we find out that the strength of an edge is 100%, then the edge remains but the label on it is removed. However, if we find out that the strength of the edge is 0%, then the edge is removed from
the network. For example, in the figure below, we find out that <b>(F, G)</b> has a true strength of 100% (which is why in the figure, the edge label was removed but the edge was kept) and <b>(F, C)</b> has a true strength of 0% (and therefore, the edge was removed). Note that the true strength can only 
take on two values: 100% or 0%. 

<div class="container">
<img src="../images/GameScreenshots/obs-1.png" />
<p align="center">Sample Selection</p>
</div>
<div class="container">
<img src="../images/GameScreenshots/obs-2.png" />
<p align="center">Observation Generated</p>
</div>


<H2><u>Goal</u></H2>
<p align="justify">In order to assist the social workers, your goal is to select intervention attendees from the network for two intervention camps organized by our social workers. You must select two circles (youth) for each intervention camp.
You should select the intervention attendees in a way such that the <b>average number of homeless youth</b> who get aware about HIV (according to the influence model described above) at the end of both intervention camps is maximized. Note that we consider the average number of homeless youth because spread 
of awareness is not a deterministic process. That is, a youth may try to spread awareness to a friend but may fail in doing so (due to the strength of his friendship not being 100%). In order to clearly explain the objective,  consider 
a simple example shown in the figure below. If you select node <b>E</b>, then with 50% chance, <b>D</b> will get aware about HIV and in this case, the value of the objective is 1+1 = 2. On the other hand, with a 50% chance, <b>E</b> may fail to spread awareness about HIV to <b>D</b>. In this case, the value 
of the objective is 1. Therefore, the average number of homeless youth who get aware about HIV by selecting circle <b>E</b> is 0.50 * 2 + 0.50*1 = 1.5. You have to select the two nodes which maximize the average value for the network that will be shown to you.</p>


<div class="container">
<img src="../images/GameScreenshots/uncert-goal.png" />
<p align="center">Random Selection of two nodes and Awareness Spread</p>
</div>

<H2><u>Instructions</u></H2>
<p align="justify">You can select a circle by clicking on it using your mouse. The color of a circle changes to red when it is selected. If you don't like your selection, and would like to modify it, you can unselect a selected node by clicking on it again. Nodes which are unselected will have white color. When you are happy with you selection, 
you can press the Submit button in order to submit your response. First, you will be shown a sample network in order to aid understanding of the game. After that, as part of the actual game, you will be shown four different networks and you will have to select a set of two circles in each of these networks. After finishing this task, you will be asked to play the second phase of the game. The instructions of the second phase will be shown to you 
after you complete the first phase of the game.</p>


<H2><u>Rewards</u></H2>
<p align="justify">You will only be paid money if you finish the entire game (both phase 1 and phase 2). You will be paid a base compensation of 50 cents for finishing both the phases of this game. In addition, on each of the four networks for which you select circles, you will be paid a bonus amount which will depend on how many youth 
(or circles) were made aware about HIV with your selection. For each youth who was made aware of HIV by your selection, you will earn 1 cent. If the objective value that you achieve is fractional, it will be rounded up to a whole cent. Thus, in Figure 4, since that random selection was able to achieve an objective value of 1.5, 
selecting that random choice will allow you to win 2 cents (rounded up from 1.5) for that network. Thus, in order to win more money, you should aim to select circles which will spread awareness about HIV to the most number of youth on average. You will also get a bonus 50 cents for finishing the second phase of the game.</p>

<!--<h4>I have read the study instructions.</h4>

	<input type="radio" name="formAgreement" id= "formAgreementTrue" value="Yes" onClick="EnableSubmit()"> Yes<br>
	<input type="radio" name="formAgreement"  id= "formAgreementfalse" value="No"  onClick="EnableSubmit()" > No<br>
	<br>    
</body>
 <script  type="text/javascript">
     function EnableSubmit()
        {
            var submit = document.getElementById("submit");
            var val=  document.getElementById("formAgreementTrue");
            if (  val.checked == true)
            {
                submit.disabled = false;
            }
            else
            {
                submit.disabled = true;
            }
        } 
 </script>

 <input type="submit" name="submit" id="submit" value="<?php echo $BtnLabel; ?>"   disabled = true/>
    </form>-->
</html>
<? ob_flush(); ?>




















