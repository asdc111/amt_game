<? ob_start();?>
<?php
//session_set_cookie_params(0);
session_start();
/*
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
    // last request was more than 30 minutes ago
    session_unset();     // unset $_SESSION variable for the run-time 
    session_destroy();   // destroy session data in storage
}
$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
*/
if(isset($_SESSION['passcode']) )
{
	session_write_close();
    //header("Location:content.php");
}
else
{
	session_write_close();
    //header("Location:index.php");
}

    
if($_SESSION['visitedSampleGame']) 
    $BtnLabel ='Start the Game'; 
else 
    $BtnLabel ='Start Sample Game'; 

?>

<html>

<head>
  <title>Teamcore Network Security Game</title>
	<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
	<link rel="icon" href="../favicon.ico" type="image/x-icon">
</head>
<body>
    <img src="../logo.jpg">
    <script type="text/javascript" src="../jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
                flag=true;
                window.onbeforeunload = function(){
                    if(flag)
                    {   
						$.get("../logout.php");
                      //  return "Warning you are still logged in, are you sure?";
                    }
                    };
</script>
        <form action="content1.php" method="post" onsubmit="flag=false;">
<H4>Phase 2 Instructions</H4>
<H2>Maximizing Influence in Multiple Steps Under No Uncertainty</H2>


<p>In the second phase of this game, you will be verifying different possible solutions for some networks. For each network, you will be shown four different settings for chosen nodes. 
    Your goal is to select the option which you think will perform best in terms of maximizing influence. 
  </p>


<p>This experiment consists of 8 rounds, and each round takes players an average of 30-50 seconds. </p>

<h4>Reward point to money conversion</h4>
<p>In each round, you will receive a base compensation of 0.50$. In addition, you will have a per-round bonus which will be determined by how close your selected nodes are to the optimal selected nodes. The value of this per-round bonus is inversely proportional to the difference in influence spread achieved by your chosen nodes and the optimal nodes. </p>

<h4>I have read the study instructions.</h4>

	<input type="radio" name="formAgreement" id= "formAgreementTrue" value="Yes" onClick="EnableSubmit()"> Yes<br>
	<input type="radio" name="formAgreement"  id= "formAgreementfalse" value="No"  onClick="EnableSubmit()" > No<br>
	<br>    
</body>
 <script  type="text/javascript">
     function EnableSubmit()
        {
            var submit = document.getElementById("submit");
            var val=  document.getElementById("formAgreementTrue");
            if (  val.checked == true)
            {
                submit.disabled = false;
            }
            else
            {
                submit.disabled = true;
            }
        } 
 </script>

 <input type="submit" name="submit" id="submit" value="<?php echo $BtnLabel; ?>"   disabled = true/>
    </form>
</html>
<? ob_flush(); ?>