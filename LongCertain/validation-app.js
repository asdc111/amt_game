// set up SVG for D3
var Counter=1;
var width = chartline1.clientWidth - 30,
      height = 600,
     colors = d3.scale.ordinal().range(["#FFFFFF"]);
     //colors = d3.scale.ordinal().range(['#20E637',"#FFFFFF",'#ffff00']);
//		colors = d3.scale.category10();

//var margin = {top: 20, right: 120, bottom: 20, left: 120};
var svg = d3.select('#chartline1')
        .append('svg')
        .attr("width", width )
        .attr("height", height);

var svg2 = d3.select('#chartline2')
        .append('svg')
        .attr("width", width )
        .attr("height", height);

var svg3 = d3.select('#chartline3')
        .append('svg')
        .attr("width", width )
        .attr("height", height);

var svg4 = d3.select('#chartline4')
        .append('svg')
        .attr("width", width )
        .attr("height", height);
	  
//var choice_nodes = [];
var selected_path = [];
//window.onload(updateData());
// set up initial nodes and links
//  - nodes are known by 'id', not by index in array.
//  - reflexive edges are indicated on the node (as a bold black circle).
//  - links are always source < target; edge directions are set by 'left' and 'right'.
//			  color: "#20E637",
          


// init D3 force layout


	  // define arrow markers for graph links
//d3.json("graph.json", function(error, data) {
//var nodes= data.nodes;
//var links= data.links;

function shuffle(array) {
  for (var i = array.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * 1000) % (i + 1);
    var temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
}

var json;

var answer_list = possible_ans.split(",");

answer_list[3] = user_selected_nodes;

shuffle(answer_list);

console.log(answer_list[0]);
console.log(answer_list[1]);
console.log(answer_list[2]);
console.log(answer_list[3]);

//test();
//d3.json("graph1.json", 
//function test(jsondata) {
var jsondata = JSON.stringify(jsondata);
json = JSON.parse(jsondata);
  var edges = [];
    json.Links.forEach(function(e) { 
    var sourceNode = json.Nodes.filter(function(n) { return n.id === e.Source; })[0],
    targetNode = json.Nodes.filter(function(n) { return n.id === e.Target; })[0];
    var linkLabel = "F:" + Math.floor(e.coveredProbability * 100) + "%;S:" + Math.floor((1.0 - e.coveredProbability) * 100) + "%";
    edges.push({source: sourceNode, target: targetNode,left : e.left, right: e.right , label: linkLabel, cost: e.cost, id: e.id, probability: e.coveredProbability});
    });
 
 colors = d3.scale.ordinal().range(["#FFFFFF"]);
 //colors = d3.scale.ordinal().range(['#20E637','#ffff00']);
//colors = d3.scale.ordinal().range(['#20E637',"#FFFFFF",'#ffff00']);
json.Nodes.forEach(function(d,i)
{

	if(d.type =='inner')
    {
        colors = d3.scale.ordinal().range(["#FFFFFF"]);
        //colors = d3.scale.ordinal().range(['#20E637',"#FFFFFF",'#ffff00']);
    }
    
});		
     

var force = d3.layout.force()
      .nodes(json.Nodes)
      .links(edges)
      .size([width, height])
      .linkDistance(200)
      .charge(-1000)
      .on('tick', tick);

svg.append('svg:defs').append('svg:marker')
      .attr('id', 'end-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 18)
      .attr('markerWidth', 3)
      .attr('markerHeight', 3)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M0,-5L10,0L0,5')
      .attr('fill', '#000');

svg.append('svg:defs').append('svg:marker')
      .attr('id', 'start-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 16)
      .attr('markerWidth', 3)
      .attr('markerHeight', 3)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M10,-5L0,0L10,5')
      .attr('fill', '#000');


var force2 = d3.layout.force()
      .nodes(json.Nodes)
      .links(edges)
      .size([width, height])
      .linkDistance(200)
      .charge(-1000)
      .on('tick', tick)

svg2.append('svg:defs').append('svg:marker')
      .attr('id', 'end-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 18)
      .attr('markerWidth', 3)
      .attr('markerHeight', 3)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M0,-5L10,0L0,5')
      .attr('fill', '#000');

svg2.append('svg:defs').append('svg:marker')
      .attr('id', 'start-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 16)
      .attr('markerWidth', 3)
      .attr('markerHeight', 3)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M10,-5L0,0L10,5')
      .attr('fill', '#000');


var force3 = d3.layout.force()
      .nodes(json.Nodes)
      .links(edges)
      .size([width, height])
      .linkDistance(200)
      .charge(-1000)
      .on('tick', tick)

svg3.append('svg:defs').append('svg:marker')
      .attr('id', 'end-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 18)
      .attr('markerWidth', 3)
      .attr('markerHeight', 3)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M0,-5L10,0L0,5')
      .attr('fill', '#000');

svg3.append('svg:defs').append('svg:marker')
      .attr('id', 'start-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 16)
      .attr('markerWidth', 3)
      .attr('markerHeight', 3)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M10,-5L0,0L10,5')
      .attr('fill', '#000');

var force4 = d3.layout.force()
      .nodes(json.Nodes)
      .links(edges)
      .size([width, height])
      .linkDistance(200)
      .charge(-1000)
      .on('tick', tick)

svg4.append('svg:defs').append('svg:marker')
      .attr('id', 'end-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 18)
      .attr('markerWidth', 3)
      .attr('markerHeight', 3)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M0,-5L10,0L0,5')
      .attr('fill', '#000');

svg4.append('svg:defs').append('svg:marker')
      .attr('id', 'start-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 16)
      .attr('markerWidth', 3)
      .attr('markerHeight', 3)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M10,-5L0,0L10,5')
      .attr('fill', '#000');






var drag = force.drag()
    .on("dragstart", dragstart);

var drag2 = force2.drag()
    .on("dragstart", dragstart);

var drag3 = force3.drag()
    .on("dragstart", dragstart);

var drag4 = force4.drag()
    .on("dragstart", dragstart);

var n = json.Nodes.length;

var x=20;
//var y= new Array(75,150,225,300,375,450,525,600);
var y= new Array(95,185,275,365,455,545);
var endCount=1;
var startCount=0;	

json.Nodes.forEach(function(d,i)
{

	/*if(d.type =='start')
		{
     
		d.x = 20;
		d.fixed = true;
        d.y = y[startCount++];
		}
	else if(d.type =='end')
		{
		d.x  = width - 20;
		d.fixed = true;
        d.y = y[endCount++];
		}*/
	//d.y =  y[Math.floor(Math.random()*y.length)];
	
} );                    
	
// line displayed when dragging new nodes
//var drag_line = svg.append('svg:path')
//      .attr('class', 'link dragline hidden')
//      .attr('d', 'M0,0L0,0');
// handles to link and node element groups
var path = svg.append('svg:g').selectAll('path'),
      circle = svg.append('svg:g').selectAll('g');

var path2 = svg2.append('svg:g').selectAll('path'),
      circle2 = svg2.append('svg:g').selectAll('g');

var path3 = svg3.append('svg:g').selectAll('path'),
      circle3 = svg3.append('svg:g').selectAll('g');

var path4 = svg4.append('svg:g').selectAll('path'),
      circle4 = svg4.append('svg:g').selectAll('g');


//Gradient definition here	  
edges.forEach(function(link)
{
	//Parse failure percentage from the label.
	//Current label [F:__%;S:__%]. where __ is probability from [0, 100]
	var failurePercentage = Number(link.label.substr(link.label.indexOf('F')+2,link.label.indexOf('%')-2));
	var successPercentage = 100 - failurePercentage;
	
	//If failurePercentage < 10, add a 0 to the decimal point
	var failureOffset;
	if(failurePercentage < 10)
	{
		failureOffset = "0." + "0" + failurePercentage;
	}
	else
	{
		failureOffset = "0." + failurePercentage;
	}
	if(failurePercentage === 100)
	{
		failureOffset = "1.0";
	}

	var successOffset;
	if(successPercentage < 10)
	{
		successOffset	= "0." + "0" + successPercentage;
	}
	else
	{
		successOffset	= "0." + successPercentage;
	}
	if(successPercentage === 100)
	{
		successOffset = "1.0";
	}
	
	var gradient = svg.append('svg:defs')
	.attr('style', '-webkit-tap-highlight-color: rgba(0, 0, 0, 0); ')
  .append('svg:linearGradient')
    .attr('id', 'grad'+link.id)
    /*.attr('x1', '0%')
    .attr('y1', '100%')
    .attr('x2', '0%')
    .attr('y2', '0%')*/
    .attr('spreadMethod', 'pad');

    gradient.append('svg:stop')
    //Define the percentage of space this gradient color should occupy
    //.attr('offset', failureOffset)
    .attr('stop-color', 'rgb(0, 0, 0)');

  var gradient2 = svg2.append('svg:defs')
  .attr('style', '-webkit-tap-highlight-color: rgba(0, 0, 0, 0); ')
  .append('svg:linearGradient')
    .attr('id', 'grad'+link.id)
    /*.attr('x1', '0%')
    .attr('y1', '100%')
    .attr('x2', '0%')
    .attr('y2', '0%')*/
    .attr('spreadMethod', 'pad');

    gradient2.append('svg:stop')
    //Define the percentage of space this gradient color should occupy
    //.attr('offset', failureOffset)
    .attr('stop-color', 'rgb(0, 0, 0)');
    //.attr('stop-opacity', 1);

      var gradient3 = svg3.append('svg:defs')
  .attr('style', '-webkit-tap-highlight-color: rgba(0, 0, 0, 0); ')
  .append('svg:linearGradient')
    .attr('id', 'grad'+link.id)
    /*.attr('x1', '0%')
    .attr('y1', '100%')
    .attr('x2', '0%')
    .attr('y2', '0%')*/
    .attr('spreadMethod', 'pad');

    gradient3.append('svg:stop')
    //Define the percentage of space this gradient color should occupy
    //.attr('offset', failureOffset)
    .attr('stop-color', 'rgb(0, 0, 0)');
 

    var gradient4 = svg4.append('svg:defs')
  .attr('style', '-webkit-tap-highlight-color: rgba(0, 0, 0, 0); ')
  .append('svg:linearGradient')
    .attr('id', 'grad'+link.id)
    /*.attr('x1', '0%')
    .attr('y1', '100%')
    .attr('x2', '0%')
    .attr('y2', '0%')*/
    .attr('spreadMethod', 'pad');

    gradient4.append('svg:stop')
    //Define the percentage of space this gradient color should occupy
    //.attr('offset', failureOffset)
    .attr('stop-color', 'rgb(0, 0, 0)');
	//Define failure color (first color) here
	/*gradient.append('svg:stop')
		//Define the percentage of space this gradient color should occupy
		.attr('offset', failureOffset)
		.attr('stop-color', 'rgb(255, 51, 51)')
		.attr('stop-opacity', 1);*/

	//Define success color (second color) here		
	/*gradient.append('svg:stop')
		.attr('offset', failureOffset)
		.attr('stop-color', 'rgb(46, 204, 113)')
		.attr('stop-opacity', 1);*/
	
});

//Text write for link here
//Multi-colored text snippet adapted from: https://groups.google.com/forum/#!topic/d3-js/d2ceKITfTx0
//var linkLabel = svg.append('svg:g');
//edges.forEach(function (link) {
	//Current label [F:__%;S:__%]. where __ is probability from [0, 100]
	//var failurePercentage = Number(link.label.substr(link.label.indexOf('F')+2,link.label.indexOf('%')-2));
	//var successPercentage = 100 - failurePercentage;
	
      /*linkLabel.append('svg:text')
            .style('font-size', '15px')
            .attr('text-anchor', 'middle')
            .attr("dy", 15)
            .append('svg:textPath');*/
      // aligns the text at the middle of the path (only with text-anchor=middle

      /*linkLabel.append('svg:text')
            .style('font-size', '15px')
            .attr('text-anchor', 'middle')
            .attr("dy", 15)
            .append('svg:textPath')
      // aligns the text at the middle of the path (only with text-anchor=middle)
				.attr('startOffset', '50%')
					.attr('xlink:href', "#" + link.id)
      // attach this label to the correct path using its id
				.append('svg:tspan')
						.style("fill", "red")
						.text('[F:' + failurePercentage + '%')
					.append('svg:tspan')
						.style("fill", "black")
						.text(';')
					.append('svg:tspan')
						.style("fill", "green")
						.text('S:' + successPercentage + '%]');*/
//});







// mouse event vars
var selected_node = null,
      selected_link = null,
      mousedown_link = null,
      mousedown_node = null,
      mouseup_node = null;

function resetMouseVars() {
      mousedown_node = null;
      mouseup_node = null;
      mousedown_link = null;
}

// update force layout (called automatically each iteration)
function tick() {
  
    circle
    .attr("cx", function(d) { return d.x = Math.max(40, Math.min(width - 40, d.x)); })
    .attr("cy", function(d) { return d.y = Math.max(40, Math.min(height - 40, d.y)); });
      // draw directed edges with proper padding from node centers
      path.attr('d', function (d) {
            var deltaX = d.target.x - d.source.x,
                  deltaY = d.target.y - d.source.y,
                  dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY),
                  normX = deltaX / dist,
                  normY = deltaY / dist,
                  sourcePadding = d.left ? 17 : 12,
                  targetPadding = d.right ? 17 : 12,
                  sourceX = d.source.x + (sourcePadding * normX),
                  sourceY = d.source.y + (sourcePadding * normY),
                  targetX = d.target.x - (targetPadding * normX),
                  targetY = d.target.y - (targetPadding * normY);
                  var dx = d.target.x - d.source.x,
                  dy = d.target.y - d.source.y,
                  dr = Math.sqrt(dx * dx + dy * dy);
                  return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
            //return 'M' + sourceX + ',' + sourceY + 'L' + targetX + ',' + targetY;
      })
      .style('fill', 'transparent');
		   
  
      circle
      .attr('transform', function (d) {
            return 'translate(' + d.x + ',' + d.y + ')';
      });

          circle2
    .attr("cx", function(d) { return d.x = Math.max(40, Math.min(width - 40, d.x)); })
    .attr("cy", function(d) { return d.y = Math.max(40, Math.min(height - 40, d.y)); });
      // draw directed edges with proper padding from node centers
      path2.attr('d', function (d) {
            var deltaX = d.target.x - d.source.x,
                  deltaY = d.target.y - d.source.y,
                  dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY),
                  normX = deltaX / dist,
                  normY = deltaY / dist,
                  sourcePadding = d.left ? 17 : 12,
                  targetPadding = d.right ? 17 : 12,
                  sourceX = d.source.x + (sourcePadding * normX),
                  sourceY = d.source.y + (sourcePadding * normY),
                  targetX = d.target.x - (targetPadding * normX),
                  targetY = d.target.y - (targetPadding * normY);
                  var dx = d.target.x - d.source.x,
                  dy = d.target.y - d.source.y,
                  dr = Math.sqrt(dx * dx + dy * dy);
                  return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
            //return 'M' + sourceX + ',' + sourceY + 'L' + targetX + ',' + targetY;
      })
      .style('fill', 'transparent');
               
  
      circle2
      .attr('transform', function (d) {
            return 'translate(' + d.x + ',' + d.y + ')';
      });

          circle3
    .attr("cx", function(d) { return d.x = Math.max(40, Math.min(width - 40, d.x)); })
    .attr("cy", function(d) { return d.y = Math.max(40, Math.min(height - 40, d.y)); });
      // draw directed edges with proper padding from node centers
      path3.attr('d', function (d) {
            var deltaX = d.target.x - d.source.x,
                  deltaY = d.target.y - d.source.y,
                  dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY),
                  normX = deltaX / dist,
                  normY = deltaY / dist,
                  sourcePadding = d.left ? 17 : 12,
                  targetPadding = d.right ? 17 : 12,
                  sourceX = d.source.x + (sourcePadding * normX),
                  sourceY = d.source.y + (sourcePadding * normY),
                  targetX = d.target.x - (targetPadding * normX),
                  targetY = d.target.y - (targetPadding * normY);
                  var dx = d.target.x - d.source.x,
                  dy = d.target.y - d.source.y,
                  dr = Math.sqrt(dx * dx + dy * dy);
                  return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
            //return 'M' + sourceX + ',' + sourceY + 'L' + targetX + ',' + targetY;
      })
      .style('fill', 'transparent');
               
  
      circle3
      .attr('transform', function (d) {
            return 'translate(' + d.x + ',' + d.y + ')';
      });

          circle4
    .attr("cx", function(d) { return d.x = Math.max(40, Math.min(width - 40, d.x)); })
    .attr("cy", function(d) { return d.y = Math.max(40, Math.min(height - 40, d.y)); });
      // draw directed edges with proper padding from node centers
      path4.attr('d', function (d) {
            var deltaX = d.target.x - d.source.x,
                  deltaY = d.target.y - d.source.y,
                  dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY),
                  normX = deltaX / dist,
                  normY = deltaY / dist,
                  sourcePadding = d.left ? 17 : 12,
                  targetPadding = d.right ? 17 : 12,
                  sourceX = d.source.x + (sourcePadding * normX),
                  sourceY = d.source.y + (sourcePadding * normY),
                  targetX = d.target.x - (targetPadding * normX),
                  targetY = d.target.y - (targetPadding * normY);
                  var dx = d.target.x - d.source.x,
                  dy = d.target.y - d.source.y,
                  dr = Math.sqrt(dx * dx + dy * dy);
                  return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
            //return 'M' + sourceX + ',' + sourceY + 'L' + targetX + ',' + targetY;
      })
      .style('fill', 'transparent');
               
  
      circle4
      .attr('transform', function (d) {
            return 'translate(' + d.x + ',' + d.y + ')';
      });

      ///////////////

      circle.selectAll('circle')
            .style('fill', function (d) {
          

          //console.log(d.id);
          var choice_nodes = [];
          var new_str = String(answer_list[0]);
          //window.alert(new_str);
          var newlist = new_str.split(":");
          //console.log(newlist[0]);
          //console.log(newlist[1]);

          for (i=0;i<newlist.length;i++)
            choice_nodes.push(newlist[i]);

          //if (contains(selected_path, d))
            if (contains(choice_nodes, d.id)) {
              if (choice_nodes.indexOf(d.id) < 2) {
                return d3.rgb('#FF0000');
              } else {
                return d3.rgb('#0000FF');
              }
            }
                   /* var n1 = d.type.localeCompare("end");
                    var n2 = d.type.localeCompare("inner");
                    var n3 = d.type.localeCompare("start");
                    if(n1==0)
                        return colors(3);
                     else if(n3==0)
                        return colors(1);
                    else(n2==0)
                        return colors(2);*/
                  return colors(d.type);
            });



            ///////////////////////////////////////////

            
      //var x = 20, y = 20;
      // update existing nodes (reflexive & selected visual states)
      circle2.selectAll('circle')
            .style('fill', function (d) {
          

          //console.log(d.id);
          var choice_nodes = [];
          var new_str = String(answer_list[1]);
          //window.alert(new_str);
          var newlist = new_str.split(":");
          //console.log(newlist[0]);
          //console.log(newlist[1]);

          for (i=0;i<newlist.length;i++)
            choice_nodes.push(newlist[i]);

          //if (contains(selected_path, d))
            if (contains(choice_nodes, d.id)) {
              if (choice_nodes.indexOf(d.id) < 2) {
                return d3.rgb('#FF0000');
              } else {
                return d3.rgb('#0000FF');
              }
            }
                   /* var n1 = d.type.localeCompare("end");
                    var n2 = d.type.localeCompare("inner");
                    var n3 = d.type.localeCompare("start");
                    if(n1==0)
                        return colors(3);
                     else if(n3==0)
                        return colors(1);
                    else(n2==0)
                        return colors(2);*/
                  return colors(d.type);
            });

            /////////////////////////////////////////////////////////////

            
      //var x = 20, y = 20;
      // update existing nodes (reflexive & selected visual states)
      circle3.selectAll('circle')
            .style('fill', function (d) {
          

          //console.log(d.id);
          var choice_nodes = [];
          var new_str = String(answer_list[2]);
          //window.alert(new_str);
          var newlist = new_str.split(":");
          //console.log(newlist[0]);
          //console.log(newlist[1]);

          for (i=0;i<newlist.length;i++)
            choice_nodes.push(newlist[i]);

          //if (contains(selected_path, d))
            if (contains(choice_nodes, d.id)) {
              if (choice_nodes.indexOf(d.id) < 2) {
                return d3.rgb('#FF0000');
              } else {
                return d3.rgb('#0000FF');
              }
            }
                   /* var n1 = d.type.localeCompare("end");
                    var n2 = d.type.localeCompare("inner");
                    var n3 = d.type.localeCompare("start");
                    if(n1==0)
                        return colors(3);
                     else if(n3==0)
                        return colors(1);
                    else(n2==0)
                        return colors(2);*/
                  return colors(d.type);
            });


            ///////////////////////////////////////////////////////

            
      //var x = 20, y = 20;
      // update existing nodes (reflexive & selected visual states)
      circle4.selectAll('circle')
            .style('fill', function (d) {
          

          //console.log(d.id);
          var choice_nodes = [];
          var new_str = String(answer_list[3]);
          //window.alert(new_str);
          var newlist = new_str.split(":");
          //console.log(newlist[0]);
          //console.log(newlist[1]);

          for (i=0;i<newlist.length;i++)
            choice_nodes.push(newlist[i]);

          //if (contains(selected_path, d))
            if (contains(choice_nodes, d.id)) {
              if (choice_nodes.indexOf(d.id) < 2) {
                return d3.rgb('#FF0000');
              } else {
                return d3.rgb('#0000FF');
              }
            }
                   /* var n1 = d.type.localeCompare("end");
                    var n2 = d.type.localeCompare("inner");
                    var n3 = d.type.localeCompare("start");
                    if(n1==0)
                        return colors(3);
                     else if(n3==0)
                        return colors(1);
                    else(n2==0)
                        return colors(2);*/
                  return colors(d.type);
            });

      
       
}

// update graph (called when needed)
function restart() {
	

      circle
    .attr("cx", function(d) { return d.x = Math.max(40, Math.min(width - 40, d.x)); })
    .attr("cy", function(d) { return d.y = Math.max(40, Math.min(height - 40, d.y)); });
      // draw directed edges with proper padding from node centers
      path.attr('d', function (d) {
            var deltaX = d.target.x - d.source.x,
                  deltaY = d.target.y - d.source.y,
                  dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY),
                  normX = deltaX / dist,
                  normY = deltaY / dist,
                  sourcePadding = d.left ? 17 : 12,
                  targetPadding = d.right ? 17 : 12,
                  sourceX = d.source.x + (sourcePadding * normX),
                  sourceY = d.source.y + (sourcePadding * normY),
                  targetX = d.target.x - (targetPadding * normX),
                  targetY = d.target.y - (targetPadding * normY);
            return 'M' + sourceX + ',' + sourceY + 'L' + targetX + ',' + targetY;
      });
       
  
      circle
      .attr('transform', function (d) {
            return 'translate(' + d.x + ',' + d.y + ')';
      });

          circle2
    .attr("cx", function(d) { return d.x = Math.max(40, Math.min(width - 40, d.x)); })
    .attr("cy", function(d) { return d.y = Math.max(40, Math.min(height - 40, d.y)); });
      // draw directed edges with proper padding from node centers
      path2.attr('d', function (d) {
            var deltaX = d.target.x - d.source.x,
                  deltaY = d.target.y - d.source.y,
                  dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY),
                  normX = deltaX / dist,
                  normY = deltaY / dist,
                  sourcePadding = d.left ? 17 : 12,
                  targetPadding = d.right ? 17 : 12,
                  sourceX = d.source.x + (sourcePadding * normX),
                  sourceY = d.source.y + (sourcePadding * normY),
                  targetX = d.target.x - (targetPadding * normX),
                  targetY = d.target.y - (targetPadding * normY);
            return 'M' + sourceX + ',' + sourceY + 'L' + targetX + ',' + targetY;
      });
               
  
      circle2
      .attr('transform', function (d) {
            return 'translate(' + d.x + ',' + d.y + ')';
      });

          circle3
    .attr("cx", function(d) { return d.x = Math.max(40, Math.min(width - 40, d.x)); })
    .attr("cy", function(d) { return d.y = Math.max(40, Math.min(height - 40, d.y)); });
      // draw directed edges with proper padding from node centers
      path3.attr('d', function (d) {
            var deltaX = d.target.x - d.source.x,
                  deltaY = d.target.y - d.source.y,
                  dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY),
                  normX = deltaX / dist,
                  normY = deltaY / dist,
                  sourcePadding = d.left ? 17 : 12,
                  targetPadding = d.right ? 17 : 12,
                  sourceX = d.source.x + (sourcePadding * normX),
                  sourceY = d.source.y + (sourcePadding * normY),
                  targetX = d.target.x - (targetPadding * normX),
                  targetY = d.target.y - (targetPadding * normY);
            return 'M' + sourceX + ',' + sourceY + 'L' + targetX + ',' + targetY;
      });
               
  
      circle3
      .attr('transform', function (d) {
            return 'translate(' + d.x + ',' + d.y + ')';
      });

          circle4
    .attr("cx", function(d) { return d.x = Math.max(40, Math.min(width - 40, d.x)); })
    .attr("cy", function(d) { return d.y = Math.max(40, Math.min(height - 40, d.y)); });
      // draw directed edges with proper padding from node centers
      path4.attr('d', function (d) {
            var deltaX = d.target.x - d.source.x,
                  deltaY = d.target.y - d.source.y,
                  dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY),
                  normX = deltaX / dist,
                  normY = deltaY / dist,
                  sourcePadding = d.left ? 17 : 12,
                  targetPadding = d.right ? 17 : 12,
                  sourceX = d.source.x + (sourcePadding * normX),
                  sourceY = d.source.y + (sourcePadding * normY),
                  targetX = d.target.x - (targetPadding * normX),
                  targetY = d.target.y - (targetPadding * normY);
            return 'M' + sourceX + ',' + sourceY + 'L' + targetX + ',' + targetY;
      });
               
  
      circle4
      .attr('transform', function (d) {
            return 'translate(' + d.x + ',' + d.y + ')';
      });

       // path (link) group
      path = path.data(edges);

      // update existing links
      path.style('marker-start', function (d) {
               //return d.left ? 'url(#start-arrow)':'';
               return d.left;
            })
            .style('marker-end', function (d) {
               //return d.right ? 'url(#end-arrow)' : '';
               return d.right;
            })
      .style('stroke',function (d) {
           return 'url(#grad'+d.id+')'})
      .style("stroke-opacity", 0.5)
      .style("stroke-width", 1.5);
    
  
      // add new links
      path.enter().append('svg:path')
            .attr('class', 'link')
            .attr('id', function (d) {
                  return d.id;
            })
           
            .style('marker-start', function (d) {
               //return d.left ? 'url(#start-arrow)':'';
               return d.left;
            })
            .style('marker-end', function (d) {
                  //return d.right ? 'url(#end-arrow)' : '';
                  return d.right;
            })
      .style('stroke',function (d) {
           return 'url(#grad'+d.id+')'})
            .on('mousedown', function (d) {
                  if (d3.event.ctrlKey) return;

                  // select link
                  mousedown_link = d;
                  if (mousedown_link === selected_link) selected_link = null;
                  else selected_link = mousedown_link;
                  selected_node = null;
                  //restart();
            })
      .style("stroke-opacity", 0.5)
      .style("stroke-width", 1.5);

      // remove old links
      path.exit().remove();


      // circle (node) group
      // NB: the function arg is crucial here! nodes are known by id, not by index!
      circle = circle.data(json.Nodes, function (d) {
            return d.id;
      });
      //var x = 20, y = 20;
      // update existing nodes (reflexive & selected visual states)
      circle.selectAll('circle')
            .style('fill', function (d) {
					

          //console.log(d.id);
          var choice_nodes = [];
          var new_str = String(answer_list[0]);
          //window.alert(new_str);
          var newlist = new_str.split(":");
          //console.log(newlist[0]);
          //console.log(newlist[1]);

          for (i=0;i<newlist.length;i++)
            choice_nodes.push(newlist[i]);

					//if (contains(selected_path, d))
            if (contains(choice_nodes, d.id))
                        return d3.rgb('#FF0000');
                   /* var n1 = d.type.localeCompare("end");
                    var n2 = d.type.localeCompare("inner");
                    var n3 = d.type.localeCompare("start");
                    if(n1==0)
                        return colors(3);
                     else if(n3==0)
                        return colors(1);
                    else(n2==0)
                        return colors(2);*/
                  return colors(d.type);
            })
            .classed('reflexive', function (d) {
                  return d.reflexive;
            })
            .call(force.drag);




      // add new nodes
      var g = circle.enter().append('svg:g');

      g.append('svg:circle')
		
            .attr('class', 'node')
            .attr('r', 20)
            .style('fill', function (d) {
				    /*var n1 = d.type.localeCompare("end");
                    var n2 = d.type.localeCompare("inner");
                    var n3 = d.type.localeCompare("start");
                    if(n1==0)
                        return colors(3);
                     else if(n3==0)
                        return colors(1);
                    else if(n2==0)
                        return colors(2);
                    else*/
                        return (d === selected_node) ? d3.rgb(colors(d.type)).brighter().toString() : colors(d.type);
            })
            .style('stroke', function (d) {
                  return d3.rgb(colors(d.type)).darker().toString();
            })
            .classed('reflexive', function (d) {
                  return d.reflexive;
            })
            .on('mouseover', function (d) {
                  if (!mousedown_node || d === mousedown_node) return;
                  // enlarge target node
                  d3.select(this).attr('transform', 'scale(1.1)');
            })
            .on('mouseout', function (d) {
                  if (!mousedown_node || d === mousedown_node) return;
                  // unenlarge target node
                  d3.select(this).attr('transform', '');
            })
            .on('mousedown', function (d) {
                  if (d3.event.ctrlKey) return;

                  // select node
                  mousedown_node = d;
                  if (mousedown_node === selected_node) selected_node = null;
                  else selected_node = mousedown_node;
                  selected_link = null;

                  // reposition drag line
                  //                  drag_line
                  //                        .style('marker-end', 'url(#end-arrow)')
                  //                        .classed('hidden', false)
                  //                        .attr('d', 'M' + mousedown_node.x + ',' + mousedown_node.y + 'L' + mousedown_node.x + ',' + mousedown_node.y);

                  //restart();
            })
            .on('mouseup', function (d) {
                  if (!mousedown_node) return;

                  // needed by FF
                  //                  drag_line
                  //                        .classed('hidden', true)
                  //                        .style('marker-end', '');

                  // check for drag-to-self
                  mouseup_node = d;
                  if (mouseup_node === mousedown_node) {
                        resetMouseVars();
                        return;
                  }

                  // unenlarge target node
                  d3.select(this).attr('transform', '');

                  // add link to graph (update if exists)
                  // NB: links are strictly source < target; arrows separately specified by booleans
                  var source, target, direction;
                  if (mousedown_node.id < mouseup_node.id) {
                        source = mousedown_node;
                        target = mouseup_node;
                        direction = 'right';
                  } else {
                        source = mouseup_node;
                        target = mousedown_node;
                        direction = 'left';
                  }

                  var link;
                  link = edges.filter(function (l) {
                        return (l.source === source && l.target === target);
                  })[0];

                  if (link) {
                        link[direction] = true;
                  } else {
                        link = {
                              source: source,
                              target: target,
                              left: false,
                              right: false
                        };
                        link[direction] = true;
                        links.push(link);
                  }

                  // select new link
                  selected_link = link;
                  selected_node = null;
                  //restart();
            });

      // show node IDs
      g.append('svg:text')
            .style('font-size', '10px')
            .attr('x', 0)
            .attr('y', 4)
            .attr('class', 'id')
            .text(function (d) {
                /*if(d.reward != "0" )
                    return d.label+'('+d.reward+')';
                else*/
                  return d.label;
            });


      // remove old nodes
      circle.exit().remove();

      // set the graph in motion
      force.start();
      for (var i=100;i>0;i--)
        force.tick();
      force.stop();

    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // path (link) group
      path2 = path2.data(edges);

      // update existing links
      path2.style('marker-start', function (d) {
               //return d.left ? 'url(#start-arrow)':'';
               return d.left;
            })
            .style('marker-end', function (d) {
               //return d.right ? 'url(#end-arrow)' : '';
               return d.right;
            })
      .style('stroke',function (d) {
           return 'url(#grad'+d.id+')'})
      .style("stroke-opacity", 0.5)
      .style("stroke-width", 1.5);
    
  
      // add new links
      path2.enter().append('svg:path')
            .attr('class', 'link')
            .attr('id', function (d) {
                  return d.id;
            })
           
            .style('marker-start', function (d) {
               //return d.left ? 'url(#start-arrow)':'';
               return d.left;
            })
            .style('marker-end', function (d) {
                  //return d.right ? 'url(#end-arrow)' : '';
                  return d.right;
            })
      .style('stroke',function (d) {
           return 'url(#grad'+d.id+')'})
            .on('mousedown', function (d) {
                  if (d3.event.ctrlKey) return;

                  // select link
                  mousedown_link = d;
                  if (mousedown_link === selected_link) selected_link = null;
                  else selected_link = mousedown_link;
                  selected_node = null;
                  //restart();
            })
      .style("stroke-opacity", 0.5)
      .style("stroke-width", 1.5);

      // remove old links
      path2.exit().remove();


      // circle2 (node) group
      // NB: the function arg is crucial here! nodes are known by id, not by index!
      circle2 = circle2.data(json.Nodes, function (d) {
            return d.id;
      });
      //var x = 20, y = 20;
      // update existing nodes (reflexive & selected visual states)
      circle2.selectAll('circle')
            .style('fill', function (d) {

             var choice_nodes = [];
          var new_str = String(answer_list[1]);
          //window.alert(new_str);
          var newlist = new_str.split(":");
          //console.log(newlist[0]);
          //console.log(newlist[1]);

          for (i=0;i<newlist.length;i++)
            choice_nodes.push(newlist[i]);
          
          //if (contains(selected_path2, d))
            if (contains(choice_nodes, d.id))
                        return d3.rgb('#FF0000');
                   
                  return colors(d.type);
            })
            .classed('reflexive', function (d) {
                  return d.reflexive;
            })
            .call(force2.drag);

 

      // add new nodes
      var g2 = circle2.enter().append('svg:g');

      g2.append('svg:circle')
    
            .attr('class', 'node')
            .attr('r', 20)
            .style('fill', function (d) {
            
                        return (d === selected_node) ? d3.rgb(colors(d.type)).brighter().toString() : colors(d.type);
            })
            .style('stroke', function (d) {
                  return d3.rgb(colors(d.type)).darker().toString();
            })
            .classed('reflexive', function (d) {
                  return d.reflexive;
            })
            .on('mouseover', function (d) {
                  if (!mousedown_node || d === mousedown_node) return;
                  // enlarge target node
                  d3.select(this).attr('transform', 'scale(1.1)');
            })
            .on('mouseout', function (d) {
                  if (!mousedown_node || d === mousedown_node) return;
                  // unenlarge target node
                  d3.select(this).attr('transform', '');
            })
            .on('mousedown', function (d) {
                  if (d3.event.ctrlKey) return;

                  // select node
                  mousedown_node = d;
                  if (mousedown_node === selected_node) selected_node = null;
                  else selected_node = mousedown_node;
                  selected_link = null;

                  // reposition drag line
                  //                  drag_line
                  //                        .style('marker-end', 'url(#end-arrow)')
                  //                        .classed('hidden', false)
                  //                        .attr('d', 'M' + mousedown_node.x + ',' + mousedown_node.y + 'L' + mousedown_node.x + ',' + mousedown_node.y);

                  //restart();
            })
            .on('mouseup', function (d) {
                  if (!mousedown_node) return;

                  // needed by FF
                  //                  drag_line
                  //                        .classed('hidden', true)
                  //                        .style('marker-end', '');

                  // check for drag-to-self
                  mouseup_node = d;
                  if (mouseup_node === mousedown_node) {
                        resetMouseVars();
                        return;
                  }

                  // unenlarge target node
                  d3.select(this).attr('transform', '');

                  // add link to graph (update if exists)
                  // NB: links are strictly source < target; arrows separately specified by booleans
                  var source, target, direction;
                  if (mousedown_node.id < mouseup_node.id) {
                        source = mousedown_node;
                        target = mouseup_node;
                        direction = 'right';
                  } else {
                        source = mouseup_node;
                        target = mousedown_node;
                        direction = 'left';
                  }

                  var link;
                  link = edges.filter(function (l) {
                        return (l.source === source && l.target === target);
                  })[0];

                  if (link) {
                        link[direction] = true;
                  } else {
                        link = {
                              source: source,
                              target: target,
                              left: false,
                              right: false
                        };
                        link[direction] = true;
                        links.push(link);
                  }

                  // select new link
                  selected_link = link;
                  selected_node = null;
                  //restart();
            });

      // show node IDs
      g2.append('svg:text')
            .style('font-size', '10px')
            .attr('x', 0)
            .attr('y', 4)
            .attr('class', 'id')
            .text(function (d) {
                
                  return d.label;
            });


      // remove old nodes
      circle2.exit().remove();

      // set the graph in motion
      force2.start();
      for (var i=100;i>0;i--)
        force2.tick();
      force2.stop();

    
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

 // path (link) group
      path3 = path3.data(edges);

      // update existing links
      path3.style('marker-start', function (d) {
               //return d.left ? 'url(#start-arrow)':'';
               return d.left;
            })
            .style('marker-end', function (d) {
               //return d.right ? 'url(#end-arrow)' : '';
               return d.right;
            })
      .style('stroke',function (d) {
           return 'url(#grad'+d.id+')'})
      .style("stroke-opacity", 0.5)
      .style("stroke-width", 1.5)
      ;
    
  
      // add new links
      path3.enter().append('svg:path')
            .attr('class', 'link')
            .attr('id', function (d) {
                  return d.id;
            })
           
            .style('marker-start', function (d) {
               //return d.left ? 'url(#start-arrow)':'';
               return d.left;
            })
            .style('marker-end', function (d) {
                  //return d.right ? 'url(#end-arrow)' : '';
                  return d.right;
            })
      .style('stroke',function (d) {
           return 'url(#grad'+d.id+')'})
            .on('mousedown', function (d) {
                  if (d3.event.ctrlKey) return;

                  // select link
                  mousedown_link = d;
                  if (mousedown_link === selected_link) selected_link = null;
                  else selected_link = mousedown_link;
                  selected_node = null;
                  //restart();
            })
      .style("stroke-opacity", 0.5)
      .style("stroke-width", 1.5);

      // remove old links
      path3.exit().remove();

      // circle3 (node) group
      // NB: the function arg is crucial here! nodes are known by id, not by index!
      circle3 = circle3.data(json.Nodes, function (d) {
            return d.id;
      });
      //var x = 20, y = 20;
      // update existing nodes (reflexive & selected visual states)
      circle3.selectAll('circle')
            .style('fill', function (d) {

              var choice_nodes = [];
          var new_str = String(answer_list[2]);
          //window.alert(new_str);
          var newlist = new_str.split(":");
          //console.log(newlist[0]);
          //console.log(newlist[1]);

          for (i=0;i<newlist.length;i++)
            choice_nodes.push(newlist[i]);
          
          //if (contains(selected_path3, d))
            if (contains(choice_nodes, d.id))
                        return d3.rgb('#FF0000');
                   
                  return colors(d.type);
            })
            .classed('reflexive', function (d) {
                  return d.reflexive;
            })
            .call(force3.drag);

 

      // add new nodes
      var g3 = circle3.enter().append('svg:g');

      g3.append('svg:circle')
    
            .attr('class', 'node')
            .attr('r', 20)
            .style('fill', function (d) {
           
                        return (d === selected_node) ? d3.rgb(colors(d.type)).brighter().toString() : colors(d.type);
            })
            .style('stroke', function (d) {
                  return d3.rgb(colors(d.type)).darker().toString();
            })
            .classed('reflexive', function (d) {
                  return d.reflexive;
            })
            .on('mouseover', function (d) {
                  if (!mousedown_node || d === mousedown_node) return;
                  // enlarge target node
                  d3.select(this).attr('transform', 'scale(1.1)');
            })
            .on('mouseout', function (d) {
                  if (!mousedown_node || d === mousedown_node) return;
                  // unenlarge target node
                  d3.select(this).attr('transform', '');
            })
            .on('mousedown', function (d) {
                  if (d3.event.ctrlKey) return;

                  // select node
                  mousedown_node = d;
                  if (mousedown_node === selected_node) selected_node = null;
                  else selected_node = mousedown_node;
                  selected_link = null;

                  // reposition drag line
                  //                  drag_line
                  //                        .style('marker-end', 'url(#end-arrow)')
                  //                        .classed('hidden', false)
                  //                        .attr('d', 'M' + mousedown_node.x + ',' + mousedown_node.y + 'L' + mousedown_node.x + ',' + mousedown_node.y);

                  //restart();
            })
            .on('mouseup', function (d) {
                  if (!mousedown_node) return;

                  // needed by FF
                  //                  drag_line
                  //                        .classed('hidden', true)
                  //                        .style('marker-end', '');

                  // check for drag-to-self
                  mouseup_node = d;
                  if (mouseup_node === mousedown_node) {
                        resetMouseVars();
                        return;
                  }

                  // unenlarge target node
                  d3.select(this).attr('transform', '');

                  // add link to graph (update if exists)
                  // NB: links are strictly source < target; arrows separately specified by booleans
                  var source, target, direction;
                  if (mousedown_node.id < mouseup_node.id) {
                        source = mousedown_node;
                        target = mouseup_node;
                        direction = 'right';
                  } else {
                        source = mouseup_node;
                        target = mousedown_node;
                        direction = 'left';
                  }

                  var link;
                  link = edges.filter(function (l) {
                        return (l.source === source && l.target === target);
                  })[0];

                  if (link) {
                        link[direction] = true;
                  } else {
                        link = {
                              source: source,
                              target: target,
                              left: false,
                              right: false
                        };
                        link[direction] = true;
                        links.push(link);
                  }

                  // select new link
                  selected_link = link;
                  selected_node = null;
                  //restart();
            });

      // show node IDs
      g3.append('svg:text')
            .style('font-size', '10px')
            .attr('x', 0)
            .attr('y', 4)
            .attr('class', 'id')
            .text(function (d) {
                
                  return d.label;
            });


      // remove old nodes
      circle3.exit().remove();

      // set the graph in motion
      force3.start();
      for (var i=100;i>0;i--)
        force3.tick();
      force3.stop();

  
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // path (link) group
      path4 = path4.data(edges);

      // update existing links
      path4.style('marker-start', function (d) {
               //return d.left ? 'url(#start-arrow)':'';
               return d.left;
            })
            .style('marker-end', function (d) {
               //return d.right ? 'url(#end-arrow)' : '';
               return d.right;
            })
      .style('stroke',function (d) {
           return 'url(#grad'+d.id+')'})
      .style("stroke-opacity", 0.5)
      .style("stroke-width", 1.5)
      ;
    
  
      // add new links
      path4.enter().append('svg:path')
            .attr('class', 'link')
            .attr('id', function (d) {
                  return d.id;
            })
           
            .style('marker-start', function (d) {
               //return d.left ? 'url(#start-arrow)':'';
               return d.left;
            })
            .style('marker-end', function (d) {
                  //return d.right ? 'url(#end-arrow)' : '';
                  return d.right;
            })
      .style('stroke',function (d) {
           return 'url(#grad'+d.id+')'})
            .on('mousedown', function (d) {
                  if (d3.event.ctrlKey) return;

                  // select link
                  mousedown_link = d;
                  if (mousedown_link === selected_link) selected_link = null;
                  else selected_link = mousedown_link;
                  selected_node = null;
                  //restart();
            })
      .style("stroke-opacity", 0.5)
      .style("stroke-width", 1.5);

      // remove old links
      path4.exit().remove();


      // circle4 (node) group
      // NB: the function arg is crucial here! nodes are known by id, not by index!
      circle4 = circle4.data(json.Nodes, function (d) {
            return d.id;
      });
      //var x = 20, y = 20;
      // update existing nodes (reflexive & selected visual states)
      circle4.selectAll('circle')
            .style('fill', function (d) {

             var choice_nodes = [];
          var new_str = String(answer_list[3]);
          //window.alert(new_str);
          var newlist = new_str.split(":");
          //console.log(newlist[0]);
          //console.log(newlist[1]);

          for (i=0;i<newlist.length;i++)
            choice_nodes.push(newlist[i]);
          
          //if (contains(selected_path4, d))
            if (contains(choice_nodes, d.id))
                        return d3.rgb('#FF0000');
                   
                  return colors(d.type);
            })
            .classed('reflexive', function (d) {
                  return d.reflexive;
            })
            .call(force4.drag);

 

      // add new nodes
      var g4 = circle4.enter().append('svg:g');

      g4.append('svg:circle')
    
            .attr('class', 'node')
            .attr('r', 20)
            .style('fill', function (d) {
            
                        return (d === selected_node) ? d3.rgb(colors(d.type)).brighter().toString() : colors(d.type);
            })
            .style('stroke', function (d) {
                  return d3.rgb(colors(d.type)).darker().toString();
            })
            .classed('reflexive', function (d) {
                  return d.reflexive;
            })
            .on('mouseover', function (d) {
                  if (!mousedown_node || d === mousedown_node) return;
                  // enlarge target node
                  d3.select(this).attr('transform', 'scale(1.1)');
            })
            .on('mouseout', function (d) {
                  if (!mousedown_node || d === mousedown_node) return;
                  // unenlarge target node
                  d3.select(this).attr('transform', '');
            })
            .on('mousedown', function (d) {
                  if (d3.event.ctrlKey) return;

                  // select node
                  mousedown_node = d;
                  if (mousedown_node === selected_node) selected_node = null;
                  else selected_node = mousedown_node;
                  selected_link = null;

                  // reposition drag line
                  //                  drag_line
                  //                        .style('marker-end', 'url(#end-arrow)')
                  //                        .classed('hidden', false)
                  //                        .attr('d', 'M' + mousedown_node.x + ',' + mousedown_node.y + 'L' + mousedown_node.x + ',' + mousedown_node.y);

                  //restart();
            })
            .on('mouseup', function (d) {
                  if (!mousedown_node) return;

                  // needed by FF
                  //                  drag_line
                  //                        .classed('hidden', true)
                  //                        .style('marker-end', '');

                  // check for drag-to-self
                  mouseup_node = d;
                  if (mouseup_node === mousedown_node) {
                        resetMouseVars();
                        return;
                  }

                  // unenlarge target node
                  d3.select(this).attr('transform', '');

                  // add link to graph (update if exists)
                  // NB: links are strictly source < target; arrows separately specified by booleans
                  var source, target, direction;
                  if (mousedown_node.id < mouseup_node.id) {
                        source = mousedown_node;
                        target = mouseup_node;
                        direction = 'right';
                  } else {
                        source = mouseup_node;
                        target = mousedown_node;
                        direction = 'left';
                  }

                  var link;
                  link = edges.filter(function (l) {
                        return (l.source === source && l.target === target);
                  })[0];

                  if (link) {
                        link[direction] = true;
                  } else {
                        link = {
                              source: source,
                              target: target,
                              left: false,
                              right: false
                        };
                        link[direction] = true;
                        links.push(link);
                  }

                  // select new link
                  selected_link = link;
                  selected_node = null;
                  //restart();
            });

      // show node IDs
      g4.append('svg:text')
            .style('font-size', '10px')
            .attr('x', 0)
            .attr('y', 4)
            .attr('class', 'id')
            .text(function (d) {
               
                  return d.label;
            });


      // remove old nodes
      circle4.exit().remove();

      // set the graph in motion
      force4.start();
      for (var i=100;i>0;i--)
        force4.tick();
      force4.stop();

}

function mousedown() {
  svg.classed('active', true);
  svg2.classed('active', true);
  svg3.classed('active', true);
  svg4.classed('active', true);

  

  /*if (selected_node) {

      if (choice_nodes.lastIndexOf(selected_node) != -1 ) {
        choice_nodes.splice(choice_nodes.lastIndexOf(selected_node),1);

      }
      else
      {
        choice_nodes.push(selected_node);
      }
       var path = "";
        choice_nodes.forEach(function (node) {
          path = path + node.label + ", ";
          });
        document.getElementById('selected-path').value = path.substring(0, path.length - 2);
        document.getElementById('selected-path').hidden=false;
  }*/

  /*circle.selectAll('circle')
            .style('fill', function (d) {
          
          //if (contains(selected_path, d))
            if (contains(choice_nodes,d))
                        return d3.rgb('#FF0000')
                  return colors(d.type);
            })
            .classed('reflexive', function (d) {
                  return d.reflexive;
            })
            .call(force.drag);*/


        ///VALUE OF K=2 FOR NOW.

      //document.write(document.getElementById('selected-path').value);

     // if (document.getElementById('net1').checked==true || document.getElementById('net2').checked==true || document.getElementById('net3').checked==true || document.getElementById('net4').checked==true)
       //   document.getElementById('submit').disabled = false;
      /*if (choice_nodes.length != 2)
      {
        document.getElementById('submit').disabled = true;
      } 
      if (choice_nodes.length == 2)
      {
        document.getElementById('submit').disabled = false;
      }*/
      
      //resetMouseVars();

      //restart();

}


/*function mousedown() {
      // prevent I-bar on drag
      //d3.event.preventDefault();

      // because :active only works in WebKit?
      svg.classed('active', true);
      //alert(mousedown_node.id);
	  if (d3.event.ctrlKey ) return;

      if (selected_node) {
            if (selected_path.length == 0) {
                  if (selected_node.type != 'start') return;
				  if (selected_path.lastIndexOf(selected_node) != 0 ) {
                        selected_path.splice(selected_path.lastIndexOf(selected_node), selected_path.length);
						
		            }      var exitFn = false;
				
                  if (exitFn) return;
                  selected_path.push(selected_node);
            } else {
				  //if ((selected_node.type == 'start') && (selected_path.indexOf(selected_node) == -1 )) return;
				  
					  if (selected_path.lastIndexOf(selected_node) >= 0 ) {
							selected_path.splice(selected_path.lastIndexOf(selected_node), selected_path.length);
					  } else {

							//if node selected is not a start node or has atleast connected (ie, it is a target node is any of the links)
							
							function Checkiflink(link, index, ar)
							{

								if (link.target == selected_node)
								{
									return (selected_path[selected_path.length - 1] == link.source) 
								 }
							 return false;
							} 	
							var exitFn = !edges.some(Checkiflink);
							

							if((selected_path.length!= 0) && (selected_node.type=='start') && (exitFn != false))
								{exitFn = true;}
							if (exitFn) 
								return;
							selected_path.push(selected_node);
					  }
				}
			   
            var path = "";
			var previousnode = "";
			var cost= 0;
            var reward = 0;
            document.getElementById('selected-path').value = "";
	       document.getElementById('submit').disabled = true;
            selected_path.forEach(function (node) {
				  
				
                  path = path + node.label + "->";
				  edges.forEach(function(link){
						if(link.target.id== node.id && link.source.id== previousnode)
						cost=parseInt(link.cost) +parseInt(cost);
						});
				  previousnode= node.id;
                if(node.type == "end")
                {
                    reward = node.reward;
                    document.getElementById('submit').disabled = false;
                }
            });
            document.getElementById('selected-path').value = path.substring(0, path.length - 2);
			document.getElementById('selected-path-cost').value = cost;
            document.getElementById('selected-path-reward').value = reward;
			resetMouseVars();
      }
      
	 
      // insert new node at point
      //  var point = d3.mouse(this),
      //      node = {id: ++lastNodeId, reflexive: false};
      //  node.x = point[0];
      //  node.y = point[1];
      //  nodes.push(node);

      restart();
}
*/


function mousemove() {
    
      //if (!mousedown_node) return;

      // update drag line
      //drag_line.attr('d', 'M' + mousedown_node.x + ',' + mousedown_node.y + 'L' + d3.mouse(this)[0] + ',' + d3.mouse(this)[1]);

      //restart();
}

function mouseup() {
      if (mousedown_node) {
            // hide drag line
            //            drag_line
            //                  .classed('hidden', true)
            //                  .style('marker-end', '');
      }

      // because :active only works in WebKit?
      svg.classed('active', false);
       svg2.classed('active', false);
        svg3.classed('active', false);
         svg4.classed('active', false);

      // clear mouse event vars
      //resetMouseVars();



}

function spliceLinksForNode(node) {
      var toSplice = links.filter(function (l) {
            return (l.source === node || l.target === node);
      });
      toSplice.map(function (l) {
            edges.splice(links.indexOf(l), 1);
      });
}

// only respond once per keydown
var lastKeyDown = -1;

function keydown() {
      d3.event.preventDefault();

      if (lastKeyDown !== -1) return;
      lastKeyDown = d3.event.keyCode;

      // ctrl
      if (d3.event.keyCode === 17) {
            circle.call(force.drag);
            svg.classed('ctrl', true);
             circle2.call(force2.drag);
           svg2.classed('ctrl', true);
             circle3.call(force3.drag);
            svg3.classed('ctrl', true);
             circle4.call(force4.drag);
            svg4.classed('ctrl', true);
      }

      if (!selected_node && !selected_link) return;
      switch (d3.event.keyCode) {
      case 8: // backspace
      case 46: // delete
            if (selected_node) {
                  json.Nodes.splice(json.Nodes.indexOf(selected_node), 1);
                  spliceLinksForNode(selected_node);
            } else if (selected_link) {
                  edges.splice(edges.indexOf(selected_link), 1);
            }
            selected_link = null;
            selected_node = null;
            restart();
            break;
      case 66: // B
            if (selected_link) {
                  // set link direction to both left and right
                  selected_link.left = true;
                  selected_link.right = true;
            }
            restart();
            break;
      case 76: // L
            if (selected_link) {
                  // set link direction to left only
                  selected_link.left = true;
                  selected_link.right = false;
            }
            restart();
            break;
      case 82: // R
            if (selected_node) {
                  // toggle node reflexivity
                  selected_node.reflexive = !selected_node.reflexive;
            } else if (selected_link) {
                  // set link direction to right only
                  selected_link.left = false;
                  selected_link.right = true;
            }
            restart();
            break;
      }
}
function contains(a, obj) {
      for (var i = 0; i < a.length; i++) {
            if (a[i] === obj) {
                  return true;
            }
      }
      return false;
}

function keyup() {
      lastKeyDown = -1;

      // ctrl
      if (d3.event.keyCode === 17) {
            circle
                  .on('mousedown.drag', null)
                  .on('touchstart.drag', null);
            svg.classed('ctrl', false);
            circle2
                  .on('mousedown.drag', null)
                  .on('touchstart.drag', null);
            svg2.classed('ctrl', false);
            circle3
                  .on('mousedown.drag', null)
                  .on('touchstart.drag', null);
            svg3.classed('ctrl', false);
            circle4
                  .on('mousedown.drag', null)
                  .on('touchstart.drag', null);
            svg4.classed('ctrl', false);
      }
}

function dragstart(d) {
  d3.select(this).classed("fixed", d.fixed = true);
}
// app starts here
svg.on('mousedown', mousedown)
      .on('mousemove', mousemove)
      .on('mouseup', mouseup);
svg2.on('mousedown', mousedown)
      .on('mousemove', mousemove)
      .on('mouseup', mouseup);
svg3.on('mousedown', mousedown)
      .on('mousemove', mousemove)
      .on('mouseup', mouseup);
svg4.on('mousedown', mousedown)
      .on('mousemove', mousemove)
      .on('mouseup', mouseup);
d3.select(window)
      .on('keydown', keydown)
      .on('keyup', keyup);
svg.classed('active', true);
  svg2.classed('active', true);
  svg3.classed('active', true);
  svg4.classed('active', true);
restart();
//restart();
   // };

