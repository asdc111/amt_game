// set up SVG for D3
var Counter=1;
var width = chartline1.clientWidth - 30,
      height = 600,
    
     colors = d3.scale.ordinal().range(['#20E637',"#FFFFFF",'#ffff00']);
//		colors = d3.scale.category10();

//var margin = {top: 20, right: 120, bottom: 20, left: 120};
var svg = d3.select('#chartline1')
        .append('svg')
			.attr("width", width )
			.attr("height", height)
		.append("g")
		  .attr("transform", "translate(70,70)");

//Grid variables
var points = [];
var pointGrid = d3.layout.grid()
  .points()
  .size([180, 180]);//For grid spacing

var selected_path = [];
//window.onload(updateData());
// set up initial nodes and links
//  - nodes are known by 'id', not by index in array.
//  - reflexive edges are indicated on the node (as a bold black circle).
//  - links are always source < target; edge directions are set by 'left' and 'right'.
//			  color: "#20E637",

// define arrow markers for graph links
//d3.json("graph.json", function(error, data) {
//var nodes= data.nodes;
//var links= data.links;

var json;
//test();
//d3.json("graph1.json", 
//function test(jsondata) {
var jsondata = JSON.stringify(jsondata);
json = JSON.parse(jsondata);
  var edges = [];
    json.Links.forEach(function(e) { 
    var sourceNode = json.Nodes.filter(function(n) { return n.id === e.Source; })[0],
    targetNode = json.Nodes.filter(function(n) { return n.id === e.Target; })[0];
    edges.push({source: sourceNode, target: targetNode,left : e.left, right: e.right , label: e.label, cost: e.cost, id: e.id});
    });

 colors = d3.scale.ordinal().range(['#20E637','#ffff00']);
//colors = d3.scale.ordinal().range(['#20E637',"#FFFFFF",'#ffff00']);
json.Nodes.forEach(function(d,i)
{

	if(d.type =='inner')
    {
        colors = d3.scale.ordinal().range(['#20E637',"#FFFFFF",'#ffff00']);
    }
    
});		

svg.append('svg:defs').append('svg:marker')
      .attr('id', 'end-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 18)
      .attr('markerWidth', 3)
      .attr('markerHeight', 3)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M0,-5L10,0L0,5')
      .attr('fill', '#000');

svg.append('svg:defs').append('svg:marker')
      .attr('id', 'start-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 16)
      .attr('markerWidth', 3)
      .attr('markerHeight', 3)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M10,-5L0,0L10,5')
      .attr('fill', '#000');

var n = json.Nodes.length;

var x=20;
//var y= new Array(75,150,225,300,375,450,525,600);
var y= new Array(95,185,275,365,455,545);
var endCount=1;
var startCount=0;	
	
var path = svg.append('svg:g').selectAll('path'),
      circle = svg.append('svg:g').selectAll('g');

//Gradient definition here	  
edges.forEach(function(link)
{
	//Parse failure percentage from the label.
	//Current label [F:__%;S:__%]. where __ is probability from [0, 100]
	var failurePercentage = Number(link.label.substr(link.label.indexOf('F')+2,link.label.indexOf('%')-2));
	var successPercentage = 100 - failurePercentage;
	
	//If failurePercentage < 10, add a 0 to the decimal point
	var failureOffset;
	if(failurePercentage < 10)
	{
		failureOffset = "0." + "0" + failurePercentage;
	}
	else
	{
		failureOffset = "0." + failurePercentage;
	}
	if(failurePercentage === 100)
	{
		failureOffset = "1.0";
	}

	var successOffset;
	if(successPercentage < 10)
	{
		successOffset	= "0." + "0" + successPercentage;
	}
	else
	{
		successOffset	= "0." + successPercentage;
	}
	if(successPercentage === 100)
	{
		successOffset = "1.0";
	}
	
	var gradient = svg.append('svg:defs')
	.attr('style', '-webkit-tap-highlight-color: rgba(0, 0, 0, 0); ')
  .append('svg:linearGradient')
    .attr('id', 'grad'+link.id)
    .attr('x1', '0%')
    .attr('y1', '100%')
    .attr('x2', '0%')
    .attr('y2', '0%')
    .attr('spreadMethod', 'pad');
 
	//Define failure color (first color) here
	gradient.append('svg:stop')
		//Define the percentage of space this gradient color should occupy
		.attr('offset', failureOffset)
		.attr('stop-color', 'rgb(255, 51, 51)')
		.attr('stop-opacity', 1);

	//Define success color (second color) here		
	gradient.append('svg:stop')
		.attr('offset', failureOffset)
		.attr('stop-color', 'rgb(46, 204, 113)')
		.attr('stop-opacity', 1);
	
});

//Text write for link here
//Multi-colored text snippet adapted from: https://groups.google.com/forum/#!topic/d3-js/d2ceKITfTx0
var linkLabel = svg.append('svg:g');
edges.forEach(function (link) {
	//Current label [F:__%;S:__%]. where __ is probability from [0, 100]
	var failurePercentage = Number(link.label.substr(link.label.indexOf('F')+2,link.label.indexOf('%')-2));
	var successPercentage = 100 - failurePercentage;
	
      linkLabel.append('svg:text')
            .style('font-size', '15px')
            .attr('text-anchor', 'middle')
            .attr("dy", 15)
            .append('svg:textPath')
      // aligns the text at the middle of the path (only with text-anchor=middle)
				.attr('startOffset', '50%')
					.attr('xlink:href', "#" + link.id)
      // attach this label to the correct path using its id
				.append('svg:tspan')
						.style("fill", "red")
						.text('[F:' + failurePercentage + '%')
					.append('svg:tspan')
						.style("fill", "black")
						.text(';')
					.append('svg:tspan')
						.style("fill", "green")
						.text('S:' + successPercentage + '%]');
});

// mouse event vars
var selected_node = null,
      selected_link = null,
      mousedown_link = null,
      mousedown_node = null,
      mouseup_node = null;

function resetMouseVars() {
      mousedown_node = null;
      mouseup_node = null;
      mousedown_link = null;
}

// update graph (called when needed)
function restart() {
      // circle (node) group
      // NB: the function arg is crucial here! nodes are known by id, not by index!
      circle = circle.data(json.Nodes, function (d) {
            return d.id;
      });
	  //Push the circle group onto the points array (each "point" is a cell)
	  points = [];//Init

	  for(var i = 0; i < n; i++)
	  {
		points.push({});		
	  }
		
      // add new nodes
 	  var g = circle.enter().append('svg:g');

      g.append('svg:circle')

            .attr('class', 'node')
            .attr('r', 30)
            .style('fill', function (d) {
                return (d === selected_node) ? d3.rgb(colors(d.type)).brighter().toString() : colors(d.type);
            })
            .style('stroke', function (d) {
                return d3.rgb(colors(d.type)).darker().toString();
            })
            .classed('reflexive', function (d) {
                return d.reflexive;
            })
           .on('mousedown', function (d) {
                  // select node
                  mousedown_node = d;
                  if (mousedown_node === selected_node) selected_node = null;
                  else selected_node = mousedown_node;
                  selected_link = null;
            })
            .on('mouseup', function (d) {
                  if (!mousedown_node) return;

                   // check for drag-to-self
                  mouseup_node = d;
                  if (mouseup_node === mousedown_node) {
                        resetMouseVars();
                        return;
                  }

                  var link;
                  link = edges.filter(function (l) {
                        return (l.source === source && l.target === target);
                  })[0];

                  if (link) {
                        link[direction] = true;
                  }

                  // select new link
                  selected_link = link;
                  selected_node = null;
            });

      // show node IDs
      g.append('svg:text')
            .style('font-size', '15px')
            .attr('x', 0)
            .attr('y', 4)
            .attr('class', 'id')
            .text(function (d) {
                if(d.reward != "0" )
                    return d.label+'('+d.reward+')';
                else
                  return d.label;
            });

      // remove old nodes
      circle.exit().remove();
	  
	  var point = circle.data(pointGrid(points));//Concat the circle's data with the pointGrid structure. This adds the nodes to the grid layout.

	  point.transition()
		.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });//This moves the nodes into position onto the grid.

     // path (link) group
      path = path.data(edges);

      // update existing links
      path
            .style('marker-start', function (d) {
               return d.left ? 'url(#start-arrow)':'';
            })
            .style('marker-end', function (d) {
               return d.right ? 'url(#end-arrow)' : '';
            })
			.style('stroke',function (d) {
				   return 'url(#grad'+d.id+')'})
			;
	
      // add new links
      path.enter().append('svg:path')
            .attr('class', 'link')
            .attr('id', function (d) {
                  return d.id;
            })
            .style('marker-start', function (d) {
               return d.left ? 'url(#start-arrow)':'';
            })
            .style('marker-end', function (d) {
                  return d.right ? 'url(#end-arrow)' : '';
            })
			.style('stroke',function (d) {
				   return 'url(#grad'+d.id+')'})
            .on('mousedown', function (d) {
                  // select link
                  mousedown_link = d;
                  if (mousedown_link === selected_link) selected_link = null;
                  else selected_link = mousedown_link;
                  selected_node = null;
            });

      // remove old links
      path.exit().remove();
}

function mousedown() {
      // because :active only works in WebKit?
      svg.classed('active', true);

      if (selected_node) {
            if (selected_path.length == 0) {
                  if (selected_node.type != 'start') return;
				  if (selected_path.lastIndexOf(selected_node) != 0 ) {
                        selected_path.splice(selected_path.lastIndexOf(selected_node), selected_path.length);
						
		            }      var exitFn = false;
				
                  if (exitFn) return;
                  selected_path.push(selected_node);
            } else {
				  //if ((selected_node.type == 'start') && (selected_path.indexOf(selected_node) == -1 )) return;
				  
					  if (selected_path.lastIndexOf(selected_node) >= 0 ) {
							selected_path.splice(selected_path.lastIndexOf(selected_node), selected_path.length);
					  } else {

							//if node selected is not a start node or has atleast connected (ie, it is a target node is any of the links)
							
							function Checkiflink(link, index, ar)
							{

								if (link.target == selected_node)
								{
									return (selected_path[selected_path.length - 1] == link.source) 
								 }
							 return false;
							} 	
							var exitFn = !edges.some(Checkiflink);
							

							if((selected_path.length!= 0) && (selected_node.type=='start') && (exitFn != false))
								{exitFn = true;}
							if (exitFn) 
								return;
							selected_path.push(selected_node);
					  }
				}
			   
            var path = "";
			var previousnode = "";
			var cost= 0;
            var reward = 0;
            document.getElementById('selected-path').value = "";
	       document.getElementById('submit').disabled = true;
            selected_path.forEach(function (node) {
				  
				
                  path = path + node.label + "->";
				  edges.forEach(function(link){
						if(link.target.id== node.id && link.source.id== previousnode)
						cost=parseInt(link.cost) +parseInt(cost);
						});
				  previousnode= node.id;
                if(node.type == "end")
                {
                    reward = node.reward;
                    document.getElementById('submit').disabled = false;
                }
            });
            document.getElementById('selected-path').value = path.substring(0, path.length - 2);
			document.getElementById('selected-path-cost').value = cost;
            document.getElementById('selected-path-reward').value = reward;
			resetMouseVars();
      }
      
      //restart();
}

function mouseup() {
      // because :active only works in WebKit?
      svg.classed('active', false);

      // clear mouse event vars
      resetMouseVars();
}

function spliceLinksForNode(node) {
      var toSplice = links.filter(function (l) {
            return (l.source === node || l.target === node);
      });
      toSplice.map(function (l) {
            edges.splice(links.indexOf(l), 1);
      });
}

function contains(a, obj) {
      for (var i = 0; i < a.length; i++) {
            if (a[i] === obj) {
                  return true;
            }
      }
      return false;
}

// app starts here
svg.on('mousedown', mousedown)
      .on('mouseup', mouseup);
restart();
   // };

