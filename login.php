<? ob_start();?>
<?php
//session_set_cookie_params(0);
session_start();

//echo $_REQUEST['formage'];
//echo $_REQUEST['formgender'];
if(($_POST['formAge']!="NotSet"))
{
    
 if($_REQUEST['formemail']!="")
    {
        checkEmail($_REQUEST['formemail']) ;
    }
     
 $_SESSION['passcode']=randomPassword();
 $_SESSION['formemail']=$_REQUEST['formemail'];
 $_SESSION['formGender']=$_POST['formGender'];
 $_SESSION['formAge']=$_POST['formAge'];
$_SESSION['visitedSampleGame'] = false;
    addUsersTofile();
	
	//Redirect to the proper experiment pool.
	$redirectLocation = processSubjectPools();
	//In case of page close, store the subject's pool location in the session.


	$redirectLocation1 = "./ShortUncertain/content.php";
	$redirectLocation2 = "./LongUncertain/content.php";
	$redirectLocation3 = "./LongCertain/content.php";
	$redirectLocation4 = "content.php";

	$chosenBagIndex = rand(0, 3);

	if ($chosenBagIndex==0)
	{
		$_SESSION['subjectPool'] = $redirectLocation1;
		session_write_close();
    	header("Location:".$redirectLocation1);
	}
	else if ($chosenBagIndex==1)
	{
		$_SESSION['subjectPool'] = $redirectLocation2;
		session_write_close();
    	header("Location:".$redirectLocation2);
	}
	else if ($chosenBagIndex==2)
	{
		$_SESSION['subjectPool'] = $redirectLocation3;
		session_write_close();
    	header("Location:".$redirectLocation3);
	}
	else if ($chosenBagIndex==3)
	{
		$_SESSION['subjectPool'] = $redirectLocation4;
		session_write_close();
    	header("Location:".$redirectLocation4);
	}


	//session_write_close();
    //header("Location:".$redirectLocation);
	
}
else
{
	session_write_close();
    header("Location:index.php");  
}

function checkEmail($email)
{
    echo $email;
    if(preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/" , $email))
    {
        list($username,$domain)=split('@',$email);

        if(!getmxrr ($domain,$mxhosts)) {
        return false;
        }
        return true;
    }
    return false;
}

function addUsersTofile()
{
         $list = array ( array( $_SESSION['passcode'],$_SESSION['formemail'],$_SESSION['formGender'],$_SESSION['formAge'],$_SESSION['formAgreement']));

		$fp = fopen(getcwd().'/UserDetails.csv', 'a');

		$Delimiter = '';
		$Separator = ',';
		foreach ($list as $fields) {
			//Code adapted from http://stackoverflow.com/questions/16942531/alternative-to-fputcsv
			//Code block uses an alternative to fputcsv if the current version of PhP does not define it.
			if (!function_exists(fputcsv)){
				 fwrite($fp, $Delimiter.
						implode($Delimiter.$Separator.$Delimiter, $fields).$Delimiter."\n");
			}
			else
			{
				fputcsv($fp, $fields);
			}
		}

		fclose($fp);

}

function processSubjectPools() {
	//Sample without replacement.
	//Read/write from sample pool file

	//CSV file format:
	//Location, open slots	
	
	//Store file in a two dimensional array
	$fileData = array();
	//Also track the number of open slots
	$totalOpenSlots = 0;
	//In addition, create a bag from which to sample a location from later
	$sampleBag = array();
	
	if (($handle = fopen("subjectPools.csv", "r")) !== FALSE) 
	{
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
		{
			$currentLocation = $data[0];
			$openSlots = $data[1];
			//Store file
			$fileData[$currentLocation] = $openSlots;
			//Track open slots
			$totalOpenSlots += $openSlots;
			//Add to bag: add location to the bag n times, where n == $openSlots
			for($slotCounter = 0; $slotCounter < $openSlots; $slotCounter++)
			{
				$sampleBag[] = $currentLocation;
			}
		}
		fclose($handle);
	}
	
	//If there are no open slots, redirect to any of the pools with equal probability
	if($totalOpenSlots <= 0)
	{
		$totalOpenSlots = 0;
		foreach ($fileData as $key => $value)
		{
			if($key != '')
			{
				$sampleBag[] = $key;
				$totalOpenSlots++;
			}
		}
	}

	//Pick a random number between 0 and total open slots-1. Pick the location from the array.
	$chosenBagIndex = rand(0, $totalOpenSlots-1);
	
	//Set the return location based on the random number
	$location = $sampleBag[$chosenBagIndex];	
	
	//Also, decrement the associated open slots counter in the file array
	$fileData[$location] = $fileData[$location] - 1;
	
	//Write the new file data to the file
	$fp = fopen('subjectPools.csv', 'w');
	
	//$Delimiter = '"';
	$Separator = ',';
	
	foreach ($fileData as $key => $value)
	{
		fwrite($fp, $key.",".$value."\n");
	}
	
	fclose($fp);
	
	return $location;
	
}

function generatePassword($length = 8) {
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }

    return $result;
}

function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

/*
if($_REQUEST['formemail']!="")
{
    checkEmail($_REQUEST['formemail']);
}
$_SESSION['passcode'] = generatePassword(8);    
echo $_SESSION['passcode'];

if(($_REQUEST['formgender']!="" && $_REQUEST['formage']!="Select a range of your age"))
{
 $_SESSION['formemail']=$_REQUEST['formemail'];
 $_SESSION['formGender']=$_REQUEST['formGender'];
 $_SESSION['formAge']=$_REQUEST['formAge'];
 //$_SESSION['formAgreement']=$_REQUEST['formAgreement'];
 //$_SESSION['formGender']=$_REQUEST['formGender'];
    addUsersTofile();
    header("Location:content.php");
 
}
else
{
    header("Location:index.php");  
}


*/
?>

<? ob_flush(); ?>