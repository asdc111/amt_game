// set up SVG for D3
var Counter=1;
var width = chartline1.clientWidth - 30,
      height = 600;
var drawNodeIDs = false;

//var margin = {top: 20, right: 120, bottom: 20, left: 120};
var svg = d3.select('#chartline1')
        .append('svg')
        .attr("width", width )
        .attr("height", height);


var choice_nodes = [];
	  
var selected_path = [];
//window.onload(updateData());
// set up initial nodes and links
//  - nodes are known by 'id', not by index in array.
//  - reflexive edges are indicated on the node (as a bold black circle).
//  - links are always source < target; edge directions are set by 'left' and 'right'.
//			  color: "#20E637",
          


// init D3 force layout
var json;
var jsondata = JSON.stringify(jsondata);
json = JSON.parse(jsondata);
  var edges = [];
    json.Links.forEach(function(e) { 
    var sourceNode = json.Nodes.filter(function(n) { return n.id === e.Source; })[0],
    targetNode = json.Nodes.filter(function(n) { return n.id === e.Target; })[0];
    edges.push({source: sourceNode, target: targetNode,left : e.left, right: e.right , label: e.label, cost: e.cost, id: e.id, displayEdgeCoverage:e.displayEdgeCoverage});
    });     

var force = d3.layout.force()
      .nodes(json.Nodes)
      .links(edges)
      .size([width, height])
      .linkDistance(150)
      .charge(-1500)
      .on('tick', tick)
/*
Arrow head definitions
svg.append('svg:defs').append('svg:marker')
      .attr('id', 'end-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 18)
      .attr('markerWidth', 3)
      .attr('markerHeight', 3)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M0,-5L10,0L0,5')
      .attr('fill', '#000');

svg.append('svg:defs').append('svg:marker')
      .attr('id', 'start-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 16)
      .attr('markerWidth', 3)
      .attr('markerHeight', 3)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M10,-5L0,0L10,5')
      .attr('fill', '#000');
*/
//Size information
var sourceAndIntermediateNodeRadius = 15;
var destinationNodeRadius = 20;

//Grid information

var n = json.Nodes.length;

var gridIntervalX = 100;
var gridIntervalY = 100;
var leftBoundaryX = 20;
var upperBoundaryY = 95;

var jitterX = 5; //Edges will not be drawn if start and end nodes have equal x or y values
var jitterY = 5;

//Node position definition
json.Nodes.forEach(function(d,i)
{
	d.fixed = true;
	d.x = (d.gridX * gridIntervalX) + (leftBoundaryX);
	d.y = (d.gridY * gridIntervalY) + (upperBoundaryY);
		
	//Jitter Function: If odd gridX value, add Y jitter + (gridX). If even x value, add Y jitter + (gridX*2). Similar for gridY value.
	if(d.gridX % 2 != 0)
	{
		var oldDY = d.y;
		d.y = d.y + jitterY + parseInt(d.gridX);
	}
	else
	{
		var oldDY = d.y;
		d.y = d.y + jitterY + parseInt(d.gridX * 2);
	}

	if(d.gridY % 2 != 0)
	{
		var oldDX = d.x
		d.x = d.x + jitterX + parseInt(d.gridY);
	}
	else
	{
		var oldDX = d.x
		d.x = d.x + jitterX + parseInt(d.gridY * 2);
	}	
} );                    
	
// handles to link and node element groups
var path = svg.append('svg:g').selectAll('path'),
      circle = svg.append('svg:g').selectAll('g');

/*
//Gradient definition here	  
edges.forEach(function(link)
{
	//Parse failure percentage from the label.
	//Current label [F:__%;S:__%]. where __ is probability from [0, 100]
	var failurePercentage = Number(link.label.substr(link.label.indexOf('F')+2,link.label.indexOf('%')-2));
	var successPercentage = 100 - failurePercentage;
	
	//If failurePercentage < 10, add a 0 to the decimal point
	var failureOffset;
	if(failurePercentage < 10)
	{
		failureOffset = "0." + "0" + failurePercentage;
	}
	else
	{
		failureOffset = "0." + failurePercentage;
	}
	if(failurePercentage === 100)
	{
		failureOffset = "1.0";
	}

	var successOffset;
	if(successPercentage < 10)
	{
		successOffset	= "0." + "0" + successPercentage;
	}
	else
	{
		successOffset	= "0." + successPercentage;
	}
	if(successPercentage === 100)
	{
		successOffset = "1.0";
	}
	
	var gradient = svg.append('svg:defs')
	//.attr('style', '-webkit-tap-highlight-color: rgba(0, 0, 0, 0); ')
  .append('svg:linearGradient')
    .attr('id', 'grad'+link.id)
    .attr('x1', '0%')
    .attr('y1', '0%')
    .attr('x2', '100%')
    .attr('y2', '0%')
    .attr('spreadMethod', 'pad');
 
	//Define failure color (first color) here
	gradient.append('svg:stop')
		//Define the percentage of space this gradient color should occupy
		.attr('offset', failureOffset)
		.attr('offset', '0%')
		.attr('stop-color', 'rgb(255, 51, 51)')
		.attr('stop-opacity', 1);

	//Define success color (second color) here		
	gradient.append('svg:stop')
		.attr('offset', failureOffset)
		//.attr('offset', '100%')
		.attr('stop-color', 'rgb(46, 204, 113)')
		.attr('stop-opacity', 1);
	
});
*/

//Text write for link here
//Multi-colored text snippet adapted from: https://groups.google.com/forum/#!topic/d3-js/d2ceKITfTx0
var linkLabel = svg.append('svg:g');
edges.forEach(function (link) {
	if(link.displayEdgeCoverage == 'true')
	{
		//Current label [F:__%;S:__%]. where __ is probability from [0, 100]
		var failurePercentage = Number(link.label.substr(link.label.indexOf('F')+2,link.label.indexOf('%')-2));
		var successPercentage = 100 - failurePercentage;
		
		  linkLabel.append('svg:text')
				.style('font-size', '15px')
				.attr('text-anchor', 'middle')
				.attr("dy", 5)
				.append('svg:textPath')
		  // aligns the text at the middle of the path (only with text-anchor=middle)
					.attr('startOffset', '50%')
						.attr('xlink:href', "#" + link.id)
		  // attach this label to the correct path using its id
					.append('svg:tspan')
							.style("fill", d3.rgb("#FF1111"))
							.text('[F:' + failurePercentage + '%')
						.append('svg:tspan')
							.style("fill", "white")
							.text(';')
						.append('svg:tspan')
							.style("fill", d3.rgb("#33FF33"))
							.text('S:' + successPercentage + '%]');
	}
});


// mouse event vars
var selected_node = null,
      selected_link = null,
      mousedown_link = null,
      mousedown_node = null,
      mouseup_node = null;

function resetMouseVars() {
      mousedown_node = null;
      mouseup_node = null;
      mousedown_link = null;
}

// update force layout (called automatically each iteration)
function tick() {
    circle
    .attr("cx", function(d) { return d.x = Math.max(40, Math.min(width - 40, d.x)); })
    .attr("cy", function(d) { return d.y = Math.max(40, Math.min(height - 40, d.y)); });
      // draw directed edges with proper padding from node centers
      path.attr('d', function (d) {
            var deltaX = d.target.x - d.source.x,
                  deltaY = d.target.y - d.source.y,
                  dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY),
                  normX = deltaX / dist,
                  normY = deltaY / dist,
                  sourcePadding = d.left ? 17 : 12,
                  targetPadding = d.right ? 17 : 12,
                  sourceX = d.source.x + (sourcePadding * normX),
                  sourceY = d.source.y + (sourcePadding * normY),
                  targetX = d.target.x - (targetPadding * normX),
                  targetY = d.target.y - (targetPadding * normY);
            return 'M' + sourceX + ',' + sourceY + 'L' + targetX + ',' + targetY;
      });
 
      circle
      .attr('transform', function (d) {
            return 'translate(' + d.x + ',' + d.y + ')';
      });      
}


function restart() {

   // circle (node) group
      // NB: the function arg is crucial here! nodes are known by id, not by index!
      circle = circle.data(json.Nodes, function (d) {
            return d.id;
      });
      //var x = 20, y = 20;
      // update existing nodes (reflexive & selected visual states)
      circle.selectAll('circle')
            .style('fill', function (d) {
          
          if (contains(selected_path, d))
                        return d3.rgb('#FFFF00');
                  return colors(d.type);
            })
            .classed('reflexive', function (d) {
                  return d.reflexive;
            })


        force.start();
}

// update graph (called when needed)
/*function restart() {
      // path (link) group
      path = path.data(edges);

      // update existing links

      // add new links
      path.enter().append('svg:path')
            .attr('class', 'link')
            .attr('id', function (d) {
                  return d.id;
            })

            .on('mousedown', function (d) {
                  // select link
                  mousedown_link = d;
                  if (mousedown_link === selected_link) selected_link = null;
                  else selected_link = mousedown_link;
                  selected_node = null;
                  restart();
            });

      // remove old links
      path.exit().remove();


      // circle (node) group
      // NB: the function arg is crucial here! nodes are known by id, not by index!
      circle = circle.data(json.Nodes, function (d) {
            return d.id;
      });
      //var x = 20, y = 20;
      // update existing nodes (reflexive & selected visual states)
      circle.selectAll('circle')
            .style('fill', function (d) {
					
					if (contains(selected_path, d))
                        return d3.rgb('#FFFF00');
                  return colors(d.type);
            })
            .classed('reflexive', function (d) {
                  return d.reflexive;
            })

      // add new nodes
      var g = circle.enter().append('svg:g');
	  
      g.append('svg:circle')
            .attr('class', 'node')
            .attr('r', function (d) {
				  return nodeSize(d.type);
            })
            .style('fill', function (d) {
				  return (d === selected_node) ? d3.rgb(colors(d.type)).brighter().toString() : colors(d.type);
            })
            .style('stroke', function (d) {
                  return d3.rgb(colors(d.type)).darker().toString();
            })
            .classed('reflexive', function (d) {
                  return d.reflexive;
            })
            .on('mouseover', function (d) {
                  if (!mousedown_node || d === mousedown_node) return;
                  // enlarge target node
                  d3.select(this).attr('transform', 'scale(1.1)');
            })
            .on('mouseout', function (d) {
                  if (!mousedown_node || d === mousedown_node) return;
                  // unenlarge target node
                  d3.select(this).attr('transform', '');
            })
            .on('mousedown', function (d) {
                  if (d3.event.ctrlKey) return;

                  // select node
                  mousedown_node = d;
                  if (mousedown_node === selected_node) selected_node = null;
                  else selected_node = mousedown_node;
                  selected_link = null;

                  // reposition drag line
                  //                  drag_line
                  //                        .style('marker-end', 'url(#end-arrow)')
                  //                        .classed('hidden', false)
                  //                        .attr('d', 'M' + mousedown_node.x + ',' + mousedown_node.y + 'L' + mousedown_node.x + ',' + mousedown_node.y);

                  restart();
            })
            .on('mouseup', function (d) {
                  if (!mousedown_node) return;

                  // check for drag-to-self
                  mouseup_node = d;
                  if (mouseup_node === mousedown_node) {
                        resetMouseVars();
                        return;
                  }

                  // unenlarge target node
                  d3.select(this).attr('transform', '');

                  // add link to graph (update if exists)
                  // NB: links are strictly source < target; arrows separately specified by booleans
                  var source, target, direction;
                  if (mousedown_node.id < mouseup_node.id) {
                        source = mousedown_node;
                        target = mouseup_node;
                        direction = 'right';
                  } else {
                        source = mouseup_node;
                        target = mousedown_node;
                        direction = 'left';
                  }

                  var link;
                  link = edges.filter(function (l) {
                        return (l.source === source && l.target === target);
                  })[0];

                  if (link) {
                        link[direction] = true;
                  } else {
                        link = {
                              source: source,
                              target: target,
                              left: false,
                              right: false
                        };
                        link[direction] = true;
                        links.push(link);
                  }

                  // select new link
                  selected_link = link;
                  selected_node = null;
                  restart();
            });

      // show node IDs
      g.append('svg:text')
            .style('font-size', '15px')
            .attr('x', 0)
            .attr('y', 4)
            .attr('class', 'id')
            .text(function (d) {
                if(d.reward != "0" )
                    return d.reward;
                else if(drawNodeIDs === true)
                  return d.id;
				else
				  return "";
            });


      // remove old nodes
      circle.exit().remove();

      // set the graph in motion
      force.start();
}*/

function mousedown() {
  svg.classed('active', true);

  var path = "";

  if (selected_node) {

      if (choice_nodes.lastIndexOf(selected_node) != 0 ) {

      }
      else
      {
        choice_nodes.push(selected_node);
        choice_nodes.forEach(function (node) {
          path = path + node.label + ", ";
          });
        document.getElementById('selected-path').value = path;
      }
  }

  ///VALUE OF K=2 FOR NOW.
  if (choice_nodes.length == 2)
    document.getElementById('submit').disabled = false;

  resetMouseVars();

  restart();
}

/*
function mousedown() {
      // prevent I-bar on drag
      //d3.event.preventDefault();

      // because :active only works in WebKit?
      svg.classed('active', true);

      if (selected_node) {
            if (selected_path.length == 0) {
                  if (selected_node.type != 'start') return;
				  if (selected_path.lastIndexOf(selected_node) != 0 ) {
                        selected_path.splice(selected_path.lastIndexOf(selected_node), selected_path.length);
						
		            }      var exitFn = false;
				
                  if (exitFn) return;
                  selected_path.push(selected_node);
            } else {
				  //if ((selected_node.type == 'start') && (selected_path.indexOf(selected_node) == -1 )) return;
				  
					  if (selected_path.lastIndexOf(selected_node) >= 0 ) {
							selected_path.splice(selected_path.lastIndexOf(selected_node), selected_path.length);
					  } else {

							//if node selected is not a start node or has atleast connected (ie, it is a target node is any of the links)
							
							function Checkiflink(link, index, ar)
							{

								if (link.target == selected_node)
								{
									return (selected_path[selected_path.length - 1] == link.source) 
								 }
							 return false;
							} 	
							var exitFn = !edges.some(Checkiflink);
							

							if((selected_path.length!= 0) && (selected_node.type=='start') && (exitFn != false))
								{exitFn = true;}
							if (exitFn) 
								return;
							selected_path.push(selected_node);
					  }
				}
			   
            var path = "";
			var previousnode = "";
			var cost= 0;
            var reward = 0;
            document.getElementById('selected-path').value = "";
	       document.getElementById('submit').disabled = true;
            selected_path.forEach(function (node) {
				  
				
                  path = path + node.label + "->";
				  edges.forEach(function(link){
						if(link.target.id== node.id && link.source.id== previousnode)
						cost=parseInt(link.cost) +parseInt(cost);
						});
				  previousnode= node.id;
                if(node.type == "end")
                {
                    reward = node.reward;
                    document.getElementById('submit').disabled = false;
                }
            });
            document.getElementById('selected-path').value = path.substring(0, path.length - 2);
			document.getElementById('selected-path-cost').value = cost;
            document.getElementById('selected-path-reward').value = reward;
			resetMouseVars();
      }
      
	 
      // insert new node at point
      //  var point = d3.mouse(this),
      //      node = {id: ++lastNodeId, reflexive: false};
      //  node.x = point[0];
      //  node.y = point[1];
      //  nodes.push(node);

      restart();
}
*/


function mousemove() {
      if (!mousedown_node) return;

      // update drag line
      //drag_line.attr('d', 'M' + mousedown_node.x + ',' + mousedown_node.y + 'L' + d3.mouse(this)[0] + ',' + d3.mouse(this)[1]);

      restart();
}

function mouseup() {
      if (mousedown_node) {
            // hide drag line
            //            drag_line
            //                  .classed('hidden', true)
            //                  .style('marker-end', '');
      }

      // because :active only works in WebKit?
      svg.classed('active', false);

      // clear mouse event vars
      resetMouseVars();
}

function spliceLinksForNode(node) {
      var toSplice = links.filter(function (l) {
            return (l.source === node || l.target === node);
      });
      toSplice.map(function (l) {
            edges.splice(links.indexOf(l), 1);
      });
}

function contains(a, obj) {
      for (var i = 0; i < a.length; i++) {
            if (a[i] === obj) {
                  return true;
            }
      }
      return false;
}

function dragstart(d) {
  d3.select(this).classed("fixed", d.fixed = true);
}
// app starts here
svg.on('mousedown', mousedown)
      .on('mousemove', mousemove)
      .on('mouseup', mouseup);
restart();
   // };

function colors(type) {
	if(type =='inner')
    {
        return "#FFFFFF";
    }
	else if(type == 'start')
	{
		return '#0000FF';
	}
	else
	{
		return '#FF6600';
	}
}

function nodeSize(type) {
	if(type =='inner')
    {
        return sourceAndIntermediateNodeRadius;
    }
	else if(type == 'start')
	{
		return sourceAndIntermediateNodeRadius;
	}
	else
	{
		return destinationNodeRadius;
	}
}